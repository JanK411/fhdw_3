package automaton;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class AutomatonTestWithLists {

	final static Automaton testitestAutomat = Automaton.createZeroAutomaton();
	final static Automaton vorlesungsAutomat = Automaton.createZeroAutomaton();
	final static Automaton vorlesungsAutomatErweitert = Automaton.createZeroAutomaton();

	@BeforeClass
	public static void setUp() throws Exception {
		final State z0 = testitestAutomat.getStartState();
		final State z1 = testitestAutomat.getEndState();
		final State z2 = State.create(testitestAutomat);
		final State z3 = State.create(testitestAutomat);
		final State z4 = State.create(testitestAutomat);

		z0.add('a', 't', z2);
		z2.add('a', 'e', z3);
		z3.add('a', 's', z4);
		z4.add('a', 't', z1);
		z1.add('a', 'i', z0);
		z1.add('a', ' ', z1);

		final State s0 = vorlesungsAutomat.getStartState();
		final State s2 = State.create(vorlesungsAutomat);
		final State s3 = State.create(vorlesungsAutomat);
		final State s1 = vorlesungsAutomat.getEndState();

		s0.add('a', 'e', s2);
		s2.add('a', 'x', s1);
		s0.add('a', 'f', s3);
		s3.add('a', 'h', s3);
		s3.add('a', 'g', s1);

		final State v0 = vorlesungsAutomatErweitert.getStartState();
		final State v1 = vorlesungsAutomatErweitert.getEndState();
		final State v2 = State.create(vorlesungsAutomatErweitert);
		final State v3 = State.create(vorlesungsAutomatErweitert);

		v0.add('a', 'e', v2);
		v0.add('a', 'f', v3);
		v2.add('a', 'x', v1);
		v3.add('a', 'h', v3);
		v3.add('a', 'g', v1);
		v1.add('a', 'n', v1);
		v1.add('a', 'b', v0);

	}

	@Test
	public void test() throws InterruptedException {
		final Set<String> expected = new LinkedHashSet<>();
		expected.add("fhhg");

		final Collection<String> output = vorlesungsAutomat.generateAllResults("aaaa");

		assertEquals(expected, output);
	}

	@Test
	public void testLong() throws InterruptedException {
		final Set<String> expected = new LinkedHashSet<>();
		expected.add("fhhhhhhhhhhhhhhhhhhhhhg");

		final Collection<String> output = vorlesungsAutomat.generateAllResults("aaaaaaaaaaaaaaaaaaaaaaa");

		assertEquals(expected, output);
	}

	@Test
	public void test2() throws InterruptedException {
		final Set<String> expected = new LinkedHashSet<>();
		expected.add("fhhg");
		expected.add("exnn");
		expected.add("fgnn");
		expected.add("fhgn");

		final Set<String> output = vorlesungsAutomatErweitert.generateAllResults("aaaa");

		assertEquals(expected, output);
	}

	@Test
	public void testEmptyOutput() throws InterruptedException {
		final Set<String> expected = new LinkedHashSet<>();

		final Set<String> output = vorlesungsAutomatErweitert.generateAllResults("aabaa");
		/*
		 * Buchstabe b nicht in Transitionen enthalten, somit output leer!
		 */
		assertEquals(expected, output);
	}

	@Test
	public void testitest() throws InterruptedException {

		final Set<String> expected = new LinkedHashSet<>();
		expected.add("test");

		assertEquals(expected, testitestAutomat.generateAllResults("aaaa"));
	}

	@Test
	public void testitest2() throws InterruptedException {

		final Set<String> expected = new LinkedHashSet<>();
		expected.add("test     ");
		expected.add("testitest");

		assertEquals(expected, testitestAutomat.generateAllResults("aaaaaaaaa"));
	}

	@Test
	public void testitestetwaslaenger() throws InterruptedException {

		final Set<String> expected = new LinkedHashSet<>();
		expected.add("test     ");
		expected.add("testitest");

		for (int i = 0; i < 100; i++) {
			testitestAutomat.generateAllResults("aaaaaaaaaaaaaa");
		}
	}

	@Test
	public void testitest3() throws InterruptedException {

		final Set<String> expected = new LinkedHashSet<>();
		expected.add("test      ");
		expected.add("testitest ");
		expected.add("test itest");

		assertEquals(expected, testitestAutomat.generateAllResults("aaaaaaaaaa"));
	}

	@Ignore
	public void testFuerDieThreadAuslastungDerEinBisschenUebertriebenIst() throws InterruptedException {
		System.out.println(testitestAutomat
				.generateAllResults("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
		/*
		 * TODO ich glaube hier laufen wir in ein Problem, weil zu viele Threads
		 * versucht werden zu starten, jedoch die Semaphore geschlossen ist, sie
		 * in den Speicher kommen und warten, und dieser Speicher dann irgendwie
		 * überläuft...
		 */
	}

}
