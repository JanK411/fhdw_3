package automaton;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

public class ConcurrentAutomatonTest {

	final static Automaton testitestAutomat = Automaton.createZeroAutomaton();
	final static Automaton vorlesungsAutomat = Automaton.createZeroAutomaton();
	final static Automaton vorlesungsAutomatErweitert = Automaton.createZeroAutomaton();

	@BeforeClass
	public static void setUp() throws Exception {
		final State z0 = testitestAutomat.getStartState();
		final State z1 = testitestAutomat.getEndState();
		final State z2 = State.create(testitestAutomat);
		final State z3 = State.create(testitestAutomat);
		final State z4 = State.create(testitestAutomat);

		z0.add('a', 't', z2);
		z2.add('a', 'e', z3);
		z3.add('a', 's', z4);
		z4.add('a', 't', z1);
		z1.add('a', 'i', z0);
		z1.add('a', ' ', z1);

		final State s0 = vorlesungsAutomat.getStartState();
		final State s2 = State.create(vorlesungsAutomat);
		final State s3 = State.create(vorlesungsAutomat);
		final State s1 = vorlesungsAutomat.getEndState();

		s0.add('a', 'e', s2);
		s2.add('a', 'x', s1);
		s0.add('a', 'f', s3);
		s3.add('a', 'h', s3);
		s3.add('a', 'g', s1);

		final State v0 = vorlesungsAutomatErweitert.getStartState();
		final State v1 = vorlesungsAutomatErweitert.getEndState();
		final State v2 = State.create(vorlesungsAutomatErweitert);
		final State v3 = State.create(vorlesungsAutomatErweitert);

		v0.add('a', 'e', v2);
		v0.add('a', 'f', v3);
		v2.add('a', 'x', v1);
		v3.add('a', 'h', v3);
		v3.add('a', 'g', v1);
		v1.add('a', 'n', v1);
		v1.add('a', 'b', v0);

	}

	@Test
	public void testLittle() throws NoOutputException {
		final Automaton a = Automaton.createZeroAutomaton();
		final State z0 = a.getStartState();
		final State z1 = a.getEndState();
		z0.add('a', 'b', z1);

		assertEquals("b", a.getOneResult("a"));
	}

	@Test
	public void testSequential() throws NoOutputException {
		final Automaton a = Automaton.createZeroAutomaton();
		final State z0 = a.getStartState();
		final State z1 = State.create(a);
		final State z2 = State.create(a);
		final State z3 = a.getEndState();

		z0.add('a', 'c', z1);
		z1.add('a', 'b', z1);
		z1.add('b', 'a', z1);
		z1.add('d', 'x', z2);
		z1.add('c', 'd', z3);

		assertEquals("cbabababababad", a.getOneResult("aababababababc"));
	}

	@Test
	public void test() throws InterruptedException, NoOutputException {
		assertEquals("fhhg", vorlesungsAutomat.getOneResult("aaaa"));
	}

	@Test
	public void testLong() throws InterruptedException, NoOutputException {
		assertEquals("fhhhhhhhhhhhhhhhhhhhhhg", vorlesungsAutomat.getOneResult("aaaaaaaaaaaaaaaaaaaaaaa"));
	}

	@Test
	public void test2() throws InterruptedException, NoOutputException {
		final Set<String> expected = new LinkedHashSet<>();
		expected.add("fhhg");
		expected.add("exnn");
		expected.add("fgnn");
		expected.add("fhgn");

		assertTrue(expected.contains(vorlesungsAutomatErweitert.getOneResult("aaaa")));
	}

	@Test
	public void testEmptyOutput() throws InterruptedException {
		try {
			vorlesungsAutomatErweitert.getOneResult("aabaa");
			fail();
		} catch (final NoOutputException e) {
			// Everything perfect :)
		}
	}

	@Test
	public void testitest() throws InterruptedException, NoOutputException {
		assertEquals("test", testitestAutomat.getOneResult("aaaa"));
	}

	@Test
	public void testitest2() throws InterruptedException, NoOutputException {
		final Set<String> expected = new LinkedHashSet<>();
		expected.add("test     ");
		expected.add("testitest");

		assertTrue(expected.contains(testitestAutomat.getOneResult("aaaaaaaaa")));
	}

	@Test
	public void testitestetwaslaenger() throws InterruptedException, NoOutputException {

		final Set<String> expected = new LinkedHashSet<>();
		expected.add("test     ");
		expected.add("testitest");

		for (int i = 0; i < 100; i++) {
			testitestAutomat.getOneResult("aaaaaaaaaaaaaa");
		}
	}

	@Test
	public void testitest3() throws InterruptedException, NoOutputException {
		final Set<String> expected = new LinkedHashSet<>();
		expected.add("test      ");
		expected.add("testitest ");
		expected.add("test itest");

		assertTrue(expected.contains(testitestAutomat.getOneResult("aaaaaaaaaa")));
	}

	@Test
	public void testFuerDieThreadAuslastungDerEinBisschenUebertriebenIst()
			throws InterruptedException, NoOutputException {
		System.out.println("lalaalalalalalalal ------------>  "
				+ testitestAutomat.getOneResult("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"));
	}

}