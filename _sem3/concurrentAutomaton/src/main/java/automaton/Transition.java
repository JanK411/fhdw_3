package automaton;

public class Transition {
	private final char inputChar;
	private final char outputChar;
	private final State right;

	private Transition(final char inputChar, final char outputChar, final State right) {
		this.inputChar = inputChar;
		this.outputChar = outputChar;
		this.right = right;
	}

	public static Transition create(final State left, final char inputChar, final char outputChar, final State right) {
		if (!left.getAutomaton().equals(right.getAutomaton())) {
			throw new UnsupportedOperationException("left and right must belong to the same automat");
		}
		Transition t = new Transition(inputChar, outputChar, right);
		left.add(t);
		return t;
	}
	
	public static Transition create(final char inputChar, final char outputChar, final State right) {
		Transition t = new Transition(inputChar, outputChar, right);
		return t;
	}

	public char getInputChar() {
		return this.inputChar;
	}

	public char getOutputChar() {
		return this.outputChar;
	}

	public State getRight() {
		return this.right;
	}

	@Override
	public String toString() {
		return "--" + this.inputChar + "--" + this.right + "-(" + this.outputChar + ")>";
	}
}
