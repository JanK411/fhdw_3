package automaton;

import java.util.LinkedHashSet;
import java.util.Set;

import lockAndBuffer.InfiniteThreadRunner;
import lockAndBuffer.Lock;
import lockAndBuffer.Terminatable;

/**
 * @author jank411
 *
 */
public class AutomatonManager {

	private static final int MAXTHREADS = 3;
	private final Automaton automaton;
	private final Set<String> outputs = new LinkedHashSet<>();

	private final Lock mutex = new Lock(false);
	private final Lock finishedLock = new Lock(true);
	private final InfiniteThreadRunner runner;
	private final Lock oneHasFinishedLock = new Lock(true);

	public AutomatonManager(final Automaton automaton) {
		this.automaton = automaton;
		this.runner = new InfiniteThreadRunner(MAXTHREADS);
	}

	public static AutomatonManager create(final Automaton automaton) {
		return new AutomatonManager(automaton);
	}

	/**
	 * lässt den Automaten aus dem Manager mit {@code input} laufen
	 * 
	 * @param input
	 *            Eingabewort
	 * @return Menge der möglichen Ausgaben
	 * @throws InterruptedException
	 */
	public Set<String> generateAllResults(final String input) throws InterruptedException {
		this.runner.start();
		this.startNewConfiguration(this.automaton.getStartState(), input, "");
		this.finishedLock.lock(); // wartet, weil initial bereits gelockt
		return this.outputs;
	}

	public String getOneResult(final String input) throws NoOutputException {
		this.runner.start();
		this.startNewConfiguration(this.automaton.getStartState(), input, "");
		this.oneHasFinishedLock.lock();
		if (this.outputs.size() == 0)
			throw new NoOutputException();
		this.runner.terminateEverythingSoftly();
		return this.outputs.iterator().next();
	}

	/**
	 * startet eine neue {@code Configuration}, welche in einem weiteren Thread
	 * laufen wird
	 * 
	 * @param state
	 *            Zustand der {@code Configuration}
	 * @param newRestWord
	 *            noch abzuarbeitendes Restwort der {@code Configuration}
	 * @param currentOutput
	 *            bereits vorhandender Output der {@code Configuration}
	 */
	public void startNewConfiguration(final State state, final String newRestWord, final String currentOutput) {
		this.mutex.lock();
		final Configuration conf = Configuration.create(state, newRestWord, currentOutput, this);

		this.runner.add(new Terminatable() {
			@Override
			public void run() {
				try {
					conf.run();
					AutomatonManager.this.notifyFinished(conf, this);
				} catch (final DeadEndException e) {
					System.out.println("(" + conf.number + ")" + "Sackgasse bei --> " + conf.toString());
					AutomatonManager.this.notifyUnsuccessfulFinish(this);
				}
				System.out.println("started configuration: " + conf.number + " --> " + conf.toString());
			}

			@Override
			public void terminateSoftly() {
				conf.terminateSoftly();
			}
		});
		this.mutex.unlock();
	}

	private void notifyUnsuccessfulFinish(final Terminatable terminatable) {
		this.mutex.lock();
		this.organizeEverythingBecauseOfRemoveOfConfiguration(terminatable);
		this.mutex.unlock();
	}

	/**
	 * benachrichtigt den Automaten, dass eine {@code Configuration} das
	 * komplette Restwort abgearbeitet hat
	 * 
	 * @param configuration
	 *            fertige Configuration
	 */
	private void notifyFinished(final Configuration configuration, final Terminatable terminatable) {
		this.mutex.lock();
		final boolean endConf = configuration.getState().equals(this.automaton.getEndState());
		System.out.println("Finished: " + configuration.number + " --> " + configuration.toString()
				+ (endConf ? " successful" : " unsuccessful"));
		if (endConf) {
			this.outputs.add(configuration.getOutput());
			this.oneHasFinishedLock.unlock();
		}
		this.organizeEverythingBecauseOfRemoveOfConfiguration(terminatable);
		this.mutex.unlock();
	}

	/**
	 * must be in critical section!
	 */
	private void organizeEverythingBecauseOfRemoveOfConfiguration(final Terminatable terminatable) {
		this.runner.notifyThreadFinished(terminatable);
		if (this.runner.hasSomethingLeft() == false) {
			this.finishedLock.unlock();
			this.oneHasFinishedLock.unlock();
		}
	}

	public Automaton getAutomaton() {
		return this.automaton;
	}

}
