package automaton;

import java.util.Collection;
import java.util.Iterator;

public class Configuration {

	static int i = 0; // TODO remove

	private State state;
	private String restWord; // TODO rename
	private String output;
	private final AutomatonManager automatonManager;

	public int number;// TODO remove

	private boolean running = true;

	private Configuration(final State state, final String restWord, final String output,
			final AutomatonManager automatonManager) {
		this.number = i++;// TODO remove
		this.state = state;
		this.restWord = restWord;
		this.output = output;
		this.automatonManager = automatonManager;
	}

	public static Configuration create(final State state, final String restWord,
			final AutomatonManager automatonManager) {
		return new Configuration(state, restWord, "", automatonManager);
	}

	/**
	 * lässt die {@code Configuration} einen Schritt durchführen, welcher das
	 * Restwort um einen Buchstaben "auffrisst" und ggf (bei
	 * nichtdeterministischen Automaten) neue {@code Configuration}-Threads
	 * startet.
	 * 
	 * @throws DeadEndException
	 *             wenn für den nächsten Buchstaben keine Übergänge mehr
	 *             definiert sind
	 */
	private void step() throws DeadEndException {
		final Collection<Transition> nextStates = this.state.getTransitionsFor(this.restWord.charAt(0));
		if (nextStates.isEmpty())
			throw new DeadEndException("Dead end at " + this.number);
		else {
			final String newRestWord = this.restWord.substring(1);

			// transforming myself
			final Iterator<Transition> iterator = nextStates.iterator();
			final Transition next = iterator.next();
			this.state = next.getRight();
			this.restWord = newRestWord;

			// starting new threads if necessary
			while (iterator.hasNext()) {
				final Transition next2 = iterator.next(); // TODO rename :D
				this.automatonManager.startNewConfiguration(next2.getRight(), newRestWord,
						this.output + next2.getOutputChar());
			}
			this.output = this.output + next.getOutputChar();
			System.out.println("(" + this.number + ") did step: " + this.toString());
		}
	}

	/**
	 * lässt die {@code Configuration} von Anfang bis Ende durchlaufen
	 * 
	 * @throws DeadEndException
	 *             wenn ein Schritt nicht durchgeführt werden kann (
	 *             {@See step()} ) //TODO irgendwie auf den anderen Javadoc
	 *             verweisen
	 */
	public void run() throws DeadEndException {
		while (this.running && this.restWord.isEmpty() == false) {
			this.step();
		}
	}

	/**
	 * prüft, ob die {@code Configuration} eine Endkonfiguration des gegebenen
	 * Automaten ist
	 * 
	 * @return true, falls ja, sonst false
	 */
	public boolean isEndConfiguration() {
		return this.state.equals(this.automatonManager.getAutomaton().getEndState());
	}

	@Override
	public String toString() {
		return "(" + this.state.toString() + "," + this.restWord + "," + this.output + ")";
	}

	/**
	 * Erstellt einen neue {@code Configuration}
	 * 
	 * @param state
	 *            Zustand der {@code Configuration}
	 * @param restWord
	 *            noch abzuarbeitendes Wort der {@code Configuration}
	 * @param currentOutput
	 *            bereits generierter Output der {@code Configuration}
	 * @param automatonManager
	 *            Manager, welcher die {@code Configuration} verwalten wird
	 * @return neue {@code Configuration}
	 */
	public static Configuration create(final State state, final String restWord, final String currentOutput,
			final AutomatonManager automatonManager) {
		return new Configuration(state, restWord, currentOutput, automatonManager);
	}

	public String getOutput() {
		return this.output;
	}

	public State getState() {
		return this.state;
	}

	public void terminateSoftly() {
		this.running = false;
	}

}
