package automaton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class State {

	private final Automaton automaton;

	private final String name;

	private final Collection<Transition> transitions;

	public static int i = 0;

	private State(final Automaton automaton) {
		this.transitions = new ArrayList<>();
		this.automaton = automaton;
		this.name = "(z" + i + ")";
		i++;
	}

	/**
	 * erstellt einen Zustand für {@code aut}
	 * 
	 * @param aut
	 *            zu erweiternder Automat
	 * @return neuer Zustand
	 */
	public static State create(final Automaton aut) {
		final State state = new State(aut);
		aut.addStateToStateSet(state);
		return state;
	}

	/**
	 * fügt eine Transition an den gegeben Zustand hinzu
	 * 
	 * @param inputChar
	 *            Eingabebuchtabe
	 * @param outputChar
	 *            Ausgabebuchstabe
	 * @param s
	 *            Zustand nach der Transition
	 */
	void add(final char inputChar, final char outputChar, final State s) {
		Transition.create(this, inputChar, outputChar, s);
	}
	
	/**
	 * fügt eine Transition an den gegebenen Zustand hinzu
	 * 
	 * @param t
	 *            Transition, die hinzugefügt werden soll
	 */
	void add(final Transition t) {
		this.transitions.add(t);
	}

	/**
	 * 
	 * Liefert alle möglichen Zustände welche mit {@code c} aus {@code this}
	 * erreicht werden.
	 * 
	 * @param c
	 *            eingegebener Buchstabe
	 * @return StateSet
	 */
	Set<Transition> getTransitionsFor(final char c) {
		final HashSet<Transition> workingTransitions = new HashSet<>();
		for (final Transition current : this.transitions) {
			if (current.getInputChar() == c) {
				workingTransitions.add(current);
			}
		}
		return workingTransitions;
	}

	public Automaton getAutomaton() {
		return this.automaton;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
