package automaton;

@SuppressWarnings("serial")
public class NoTerminationException extends Exception {

	public NoTerminationException(final String message) {
		super(message);
	}

}
