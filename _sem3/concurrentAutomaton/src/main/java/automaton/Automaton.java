package automaton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class Automaton {
	private final Collection<State> states;
	private State startState;
	private State endState;

	private Automaton(final Collection<State> states, final State startState, final State endState) {
		this.startState = startState;
		this.endState = endState;
		this.states = states;
	}

	/**
	 * @return Startzustand des Automaten
	 */
	public State getStartState() {
		return this.startState;
	}

	/**
	 * ändert den Startzustand des Automaten auf {@code startState}
	 * 
	 * @param startState
	 *            neuer Startzustand
	 */
	public void setStartState(final State startState) {
		if (startState.equals(this.endState))
			throw new IllegalArgumentException("Start state and End state have to be different!");
		this.startState = startState;
	}

	/**
	 * @return Endzustand des Automaten
	 */
	public State getEndState() {
		return this.endState;
	}

	/**
	 * ändert den Endzustand des Automaten auf {@code endState}
	 * 
	 * @param endState
	 *            neuer Endzustand
	 */
	public void setEndState(final State endState) {
		if (endState.equals(this.startState))
			throw new IllegalArgumentException("Start state and End state have to be different!");
		this.endState = endState;
	}

	/**
	 * @return Nullautomat
	 */
	public static Automaton createZeroAutomaton() {
		final Collection<State> stateSet = new ArrayList<>();
		final Automaton aut = new Automaton(stateSet, null, null);

		final State startState = State.create(aut);
		aut.setStartState(startState);

		final State endState = State.create(aut);
		aut.setEndState(endState);

		stateSet.add(startState);
		stateSet.add(endState);

		return aut;
	}

	@Override
	public String toString() {
		// TODO was nettes ausdenken :D
		return "";
	}

	/**
	 * fügt {@code state} dem Automaten hinzu
	 * 
	 * @param state
	 *            hinzuzufügender Zustand
	 */
	void addStateToStateSet(final State state) {
		this.states.add(state);
	}

	public Set<String> generateAllResults(final String input) throws InterruptedException {
		final AutomatonManager manager = new AutomatonManager(this);
		return manager.generateAllResults(input);
	}

	public String getOneResult(final String input) throws NoOutputException {
		final AutomatonManager manager = new AutomatonManager(this);
		return manager.getOneResult(input);
	}

}
