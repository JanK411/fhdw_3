package automaton;

@SuppressWarnings("serial")
public class DeadEndException extends Exception {

	public DeadEndException(final String string) {
		super(string);
	}

}
