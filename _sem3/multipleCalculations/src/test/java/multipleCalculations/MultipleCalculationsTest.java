package multipleCalculations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.ErrorElementException;
import lockAndBuffer.Buffer.StoppException;
import multipleCalculations.expressions.Add;
import multipleCalculations.expressions.Constant;
import multipleCalculations.expressions.Div;
import multipleCalculations.expressions.Mul;
import multipleCalculations.expressions.StackedNumbers;

public class MultipleCalculationsTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@Test
	public void testHirarchy() throws StoppException, ErrorElementException {
		final Buffer<Integer> resultsBuffer = new Buffer<>(Arrays.asList(12, 20, 30, 42, 56));

		final Constant c2 = new Constant(2);
		final Constant c3 = new Constant(3);
		final StackedNumbers stack1 = new StackedNumbers(1, 2, 3, 4, 5);
		final StackedNumbers stack2 = new StackedNumbers(1, 2, 3, 4, 5);

		final Add add1 = new Add(c2, stack1);
		final Add add2 = new Add(c3, stack2);
		final Add add3 = new Add(c2, c3);
		final Mul mul = new Mul(add1, add2);

		mul.startCalculating();
		add1.startCalculating();
		add2.startCalculating();
		add3.startCalculating();

		for (int i = 0; i < 5; i++) {
			assertEquals(resultsBuffer.get(), mul.getOutputBuffer().get());
		}

		try {
			assertEquals(resultsBuffer.get(), mul.getOutputBuffer().get());
			fail();
		} catch (final Exception e) {
			// expected Exception
		}
	}

	@Test
	public void testStackedConstants() throws StoppException, ErrorElementException {
		final Buffer<Integer> resultsBuffer = new Buffer<>(Arrays.asList(5, 6, 7, 8));

		final Constant c2 = new Constant(2);
		final StackedNumbers stack = new StackedNumbers(3, 4, 5, 6);
		final Add add = new Add(c2, stack);

		add.startCalculating();

		for (int i = 0; i < 4; i++) {
			assertEquals(resultsBuffer.get(), add.getOutputBuffer().get());
		}
	}

	@Test
	public void testDivisionByZero() throws StoppException, ErrorElementException {
		final Constant c2 = new Constant(2);
		final StackedNumbers stack = new StackedNumbers(3, 2, 1, 0, -1);
		final Div div = new Div(c2, stack);

		div.startCalculating();

		final Buffer<Integer> resultsBuffer = new Buffer<>(5);
		resultsBuffer.put(2 / 3);
		resultsBuffer.put(2 / 2);
		resultsBuffer.put(2 / 1);
		resultsBuffer.putErrorElement("Division by Zero!!");
		resultsBuffer.put(2 / -1);

		assertEquals(resultsBuffer.get(), div.getOutputBuffer().get());
		assertEquals(resultsBuffer.get(), div.getOutputBuffer().get());
		assertEquals(resultsBuffer.get(), div.getOutputBuffer().get());
		try {
			resultsBuffer.get();
			fail();
		} catch (final ErrorElementException e) {
			// expected Exception
		}
		try {
			div.getOutputBuffer().get();
			fail();
		} catch (final ErrorElementException e) {
			// expected Exception
		}

		assertEquals(resultsBuffer.get(), div.getOutputBuffer().get());
	}

}
