package multipleCalculations.expressions;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.ErrorElementException;
import lockAndBuffer.Buffer.StoppException;

public abstract class CalcExpression implements Expression {

	private final Buffer<Integer> outputBuffer;
	private final Buffer<Integer> b1;
	private final Buffer<Integer> b2;

	public CalcExpression(final Buffer<Integer> outputBuffer, final Expression e1, final Expression e2) {
		this.outputBuffer = outputBuffer;
		this.b1 = e1.getOutputBuffer();
		this.b2 = e2.getOutputBuffer();
	}

	public void startCalculating() {
		new Thread(() -> {
			boolean running = true;
			while (running) {
				try {
					this.doCalculation();
				} catch (final StoppException e) {
					running = false;
					outputBuffer.stopp();
				}
			}
		}).start();
	}

	private void doCalculation() throws StoppException {
		try {
			this.outputBuffer.put(concreteCalc(b1.get(), b2.get()));
		} catch (final ErrorElementException e) {
			outputBuffer.putErrorElement(e.getMessage());
		}
	}

	protected abstract Integer concreteCalc(Integer first, Integer second) throws ErrorElementException;

	@Override
	public Buffer<Integer> getOutputBuffer() {
		return outputBuffer;
	}
}
