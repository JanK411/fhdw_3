package multipleCalculations.expressions;

import lockAndBuffer.Buffer;

public class Mul extends CalcExpression {
	public Mul(final Expression e1, final Expression e2) {
		super(new Buffer<Integer>(CAPACITY), e1, e2);
	}

	@Override
	protected Integer concreteCalc(final Integer first, final Integer second) {
		return first * second;
	}
}
