package multipleCalculations.expressions;

import lockAndBuffer.Buffer;
import lockAndBuffer.ConstantBuffer;

public class Constant implements Expression {

	private static final int CAPACITY = 10;
	private final Buffer<Integer> outputBuffer;

	public Constant(final int value) {
		this.outputBuffer = new ConstantBuffer<>(value, CAPACITY);
	}

	@Override
	public Buffer<Integer> getOutputBuffer() {
		return this.outputBuffer;
	}

}
