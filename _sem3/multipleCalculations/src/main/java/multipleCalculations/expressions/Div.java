package multipleCalculations.expressions;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.ErrorElementException;

public class Div extends CalcExpression {
	public Div(final Expression e1, final Expression e2) {
		super(new Buffer<Integer>(CAPACITY), e1, e2);
	}

	@Override
	protected Integer concreteCalc(final Integer first, final Integer second) throws ErrorElementException{
		if (second.equals(0)){
			throw new ErrorElementException("Division by Zero!!");
		}
		return first / second;
	}
}
