package multipleCalculations.expressions;

import lockAndBuffer.Buffer;

public class Add extends CalcExpression {

	public Add(final Expression e1, final Expression e2) {
		super(new Buffer<Integer>(CAPACITY), e1, e2);
	}

	@Override
	protected Integer concreteCalc(final Integer first, final Integer second) {
		return first + second;
	}

}
