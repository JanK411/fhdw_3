package multipleCalculations.expressions;

import lockAndBuffer.Buffer;

public interface Expression {

	static final int CAPACITY = 10;

	Buffer<Integer> getOutputBuffer();

}
