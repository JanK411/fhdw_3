package multipleCalculations.expressions;

import lockAndBuffer.Buffer;

public class StackedNumbers implements Expression {

	Buffer<Integer> outputBuffer;

	public StackedNumbers(final int... values) {
		this.outputBuffer = new Buffer<>(values.length+1);
		for (final int i : values) {
			outputBuffer.put(i);
		}
		outputBuffer.stopp();
	}

	@Override
	public Buffer<Integer> getOutputBuffer() {
		return outputBuffer;
	}

}
