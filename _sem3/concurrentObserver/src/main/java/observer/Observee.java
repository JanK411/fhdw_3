
package observer;

import java.util.HashSet;
import java.util.Set;

abstract public class Observee {

	private final Set<Observer> observers;

	protected Observee() {
		this.observers = new HashSet<>();
	}

	/**
	 * Attaches an observer to this observee. The same observer can only be
	 * registered once.
	 * 
	 * @param observer
	 *            receives update notifications until deregister is called for
	 *            this observer.
	 */
	public void register(final Observer observer) {
		this.observers.add(observer);
	}

	/**
	 * Detaches an observer from this observee.
	 * 
	 * @param observer
	 *            does no longer get update notifications until deregister is
	 *            called for this observer.
	 */
	public void deregister(final Observer observer) {
		this.observers.remove(observer);
	}

	/**
	 * Sends update notifications to all registered observers
	 */
	protected void notifyObservers() {
		observers.forEach(o -> o.update());
	}
}
