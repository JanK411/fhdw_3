package model;

public class ConcreteObserverParallel extends ConcreteObserver {

	public static ConcreteObserver create(final ConcreteObserverViewer view) {
		return new ConcreteObserverParallel(view);
	}

	private final Thread myUpdateThread;
	private boolean updateNeeded = true;

	protected ConcreteObserverParallel(final ConcreteObserverViewer view) {
		super(view);
		this.myUpdateThread = new Thread(() -> {
			while (true) {
				if (updateNeeded) {
					updateNeeded = false;
					doTheUpdate();
				} else {
					synchronized (this) {
						try {
							this.wait();
						} catch (final InterruptedException e) {
						}
					}
				}
			}
		});
		myUpdateThread.start();
	}

	@Override
	public void update() {
		updateNeeded = true;
		synchronized (this) {
			this.notify();
		}
	}

}
