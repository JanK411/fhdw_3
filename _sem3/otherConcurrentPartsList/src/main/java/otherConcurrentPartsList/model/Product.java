package otherConcurrentPartsList.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.StoppException;
import util.exceptions.CycleException;

public class Product extends ComponentCommon {
	private static final String CycleMessage = "Zyklen sind in der Aufbaustruktur nicht erlaubt!";

	public static Product create(final String name, final int price) {
		return new Product(name, price, new HashMap<String, QuantifiedComponent>());
	}

	private final HashMap<String, QuantifiedComponent> components;

	protected Product(final String name, final int price, final HashMap<String, QuantifiedComponent> components) {
		super(name, price);
		this.components = components;
	}

	@Override
	public void addPart(final Component part, final int amount) {
		if (part.contains(this))
			throw new CycleException(CycleMessage);
		final String partName = part.getName();
		if (this.getComponents().containsKey(partName)) {
			final QuantifiedComponent oldQuantification = this.getComponents().get(partName);
			oldQuantification.addQuantity(amount);
		} else {
			this.getComponents().put(partName, QuantifiedComponent.createQuantifiedComponent(amount, part));
		}
	}

	private HashMap<String, QuantifiedComponent> getComponents() {
		return this.components;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public synchronized boolean contains(final Component component) {
		if (this.equals(component))
			return true;

		final Buffer<Boolean> results = new Buffer<>(this.components.size());

		final Iterator<QuantifiedComponent> i = this.getComponents().values().iterator();
		while (i.hasNext()) {
			final QuantifiedComponent current = i.next();

			new Thread(() -> {
				results.put(current.contains(component));
			}).start();
		}
		return this.checkIfSomethingIsTrue(results);
	}

	private boolean checkIfSomethingIsTrue(final Buffer<Boolean> result) {
		for (int i = 0; i < this.components.size(); i++) {
			try {
				if (result.get())
					return true;
			} catch (final StoppException e) {
				throw new Error("passiert nicht, weil kein Stoppelement geputtet wird", e);
			}
		}
		return false;
	}

	@Override
	public Vector<QuantifiedComponent> getDirectParts() {
		return new Vector<QuantifiedComponent>(this.getComponents().values());
	}

	@Override
	synchronized public int getNumberOfMaterials() {
		int result = 0;
		Buffer<Integer> materialNumbers = new Buffer<>(this.components.size());
		final Iterator<QuantifiedComponent> iterator = this.getComponents().values().iterator();
		while (iterator.hasNext()) {
			final QuantifiedComponent current = iterator.next();
			new Thread(()->{
				materialNumbers.put(current.getNumberOfMaterials());		
			}).start();
		}
		for(int i = 0; i < components.size(); i++)
		{
			try {
				result += materialNumbers.get();
			} catch (StoppException e) {
				throw new Error("Kann nicht passieren, weil keine Stop-Elemente enthalten sein können", e);
			}
		}
		return result;
	}

	@Override
	public int getOverallPrice() {
		int ret = this.price;
		final Collection<QuantifiedComponent> qComponents = this.components.values();
		for (final QuantifiedComponent current : qComponents) {
			ret += current.getPrice();
		}
		return ret;
	}

	@Override
	public void accept(final ComponentVisitor v) {
		v.handle(this);
	}

	@Override
	synchronized public MaterialList getMaterialList() {
		final MaterialList materialList = MaterialList.create();
		Buffer<MaterialList> materialLists = new Buffer<>(this.components.size());
		for (final QuantifiedComponent current : this.components.values()) {
			new Thread(() -> {
				current.getComponent().accept(new ComponentVisitor() {
					@Override
					public void handle(final Product product) {
						materialLists.put(product.getMaterialList());
					}

					@Override
					public void handle(final Material material) {
						MaterialList m = MaterialList.create();
						m.add(material, current.getQuantity());
						materialLists.put(m);
					}
				});
			}).start();
		}
		for (int i = 0; i < this.components.size(); i++) {
			try {
				materialList.add(materialLists.get());
			} catch (StoppException e) {
				throw new Error("Kann nicht passieren, weil keine Stop-Elemente enthalten sein können", e);
			}
		}
		return materialList;
	}
}
