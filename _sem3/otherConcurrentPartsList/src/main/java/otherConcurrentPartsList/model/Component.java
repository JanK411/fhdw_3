package otherConcurrentPartsList.model;

import java.util.Vector;

public interface Component {
	/**
	 * * Adds amount pieces of the component part as subparts of the receiver. *
	 * * @throws Exception * If adding part as subpart of whole violates the
	 * hierarchy * contraint of the partslist represented by the receiver.
	 */
	public void addPart(Component part, int amount) throws UnknownComponentException;
	
	/**
	 * * Returns true if and only if the receiver directly or indirectly
	 * contains * the given component.
	 */
	public boolean contains(Component component);
	
	/**
	 * * Returns the list of all quantified parts that are direct sub-parts of
	 * the * receiver.
	 */
	public Vector<QuantifiedComponent> getDirectParts();
	
	/**
	 * * Returns the total number of all materials that are directly or
	 * indirectly * parts of the receiver.
	 */
	public int getNumberOfMaterials();
	
	public String getName();
	
	public int getOverallPrice();
	
	public void changePrice(int newPrice);
	
	public void accept(ComponentVisitor v);
	
	public MaterialList getMaterialList();
}
