package transaction.model;

public class Transfer implements TransferOrTransaction {

	public static Transfer create(final Account from, final Account to, final long amount, final String purpose) {
		return new Transfer(from, to, amount, purpose);
	}

	final private Account fromAccount;
	final private Account toAccount;
	private long amount;
	final private String purpose;

	public Transfer(final Account from, final Account to, final long amount, final String purpose) {
		this.fromAccount = from;
		this.toAccount = to;
		this.amount = amount;
		this.purpose = purpose;
	}

	public Account getFromAccount() {
		return fromAccount;
	}

	public Account getToAccount() {
		return toAccount;
	}

	public long getAmount() {
		return amount;
	}

	public String getPurpose() {
		return this.purpose;
	}

	@Override
	public void book() {
		if (amount < 0) {
			amount = -amount;
		}

		fromAccount.book(DebitEntry.create(this));
		toAccount.book(CreditEntry.create(this));

		System.out.println("Booking of transfer finished!");
	}

}
