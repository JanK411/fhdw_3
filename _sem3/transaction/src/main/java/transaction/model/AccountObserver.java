package transaction.model;

public interface AccountObserver {

	void update();

}
