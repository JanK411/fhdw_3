package transaction.model;

import java.util.LinkedList;
import java.util.List;

public class Transaction implements TransferOrTransaction {

	public static Transaction create() {
		return new Transaction();
	}

	private final List<Transfer> transfers;

	private Transaction() {
		this.transfers = new LinkedList<>();
	}

	public void addTransfer(final Transfer transfer) {
		this.transfers.add(transfer);
	}

	public List<Transfer> getTransfers() {
		return this.transfers;
	}

	@Override
	public void book() {
		transfers.forEach(transfer -> transfer.book());
		System.out.println("Booking of transaction finished!");

	}
}
