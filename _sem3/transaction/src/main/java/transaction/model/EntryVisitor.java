package transaction.model;

public interface EntryVisitor<T> {

	T handleDebitEntry(DebitEntry debitEntry);

	T handleCreditEntry(CreditEntry creditEntry);

}
