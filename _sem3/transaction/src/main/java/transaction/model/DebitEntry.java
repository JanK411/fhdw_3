package transaction.model;

public class DebitEntry extends Entry {

	public DebitEntry(final Transfer transfer) {
		super(transfer);
	}

	@Override
	public <T> T acceptEntryVisitor(final EntryVisitor<T> visitor) {
		return visitor.handleDebitEntry(this);
	}

	@Override
	public String toString() {
		return "Auszahlung über " + transfer.getAmount() + "€ an Konto " + transfer.getToAccount() + ": \""
				+ transfer.getPurpose() + "\"";
	}

	public static DebitEntry create(final Transfer transfer) {
		return new DebitEntry(transfer);
	}

}
