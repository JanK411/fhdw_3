package transaction.model;

import java.util.Map;
import java.util.TreeMap;

public class AccountManager {

	private static AccountManager theAccountManager = null;

	public static AccountManager getTheAccountManager() {
		if (theAccountManager == null) {
			theAccountManager = new AccountManager();
		}
		return theAccountManager;
	}

	private final Map<String, Account> accounts;

	private AccountManager() {
		this.accounts = new TreeMap<>();
	}

	public Account find(final String name) throws AccountNotFoundException {
		final Account result = this.accounts.get(name);
		if (result == null) {
			throw new AccountNotFoundException(name);
		}
		return result;
	}

	public Account create(final String name) throws AccountException {
		this.checkName(name);
		final Account result = this.accounts.get(name);
		if (result != null) {
			throw new AccountAlreadyExistsException(name);
		}
		final Account newAccount = Account.create(name);
		this.accounts.put(name, newAccount);
		return newAccount;
	}

	private void checkName(final String name) throws AccountException {
		if (name.length() < 3) {
			throw new AccountNameFormatException(name);
		}
	}
}
