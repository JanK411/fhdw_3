package transaction.model;

public class CreditEntry extends Entry {

	public CreditEntry(final Transfer transfer) {
		super(transfer);
	}

	@Override
	public <T> T acceptEntryVisitor(final EntryVisitor<T> visitor) {
		return visitor.handleCreditEntry(this);
	}

	@Override
	public String toString() {
		return "Einzahlung " + transfer.getAmount() + "€ von Konto " + transfer.getFromAccount() + ": \""
				+ transfer.getPurpose() + "\"";
	}

	public static CreditEntry create(final Transfer transfer) {
		return new CreditEntry(transfer);
	}

}
