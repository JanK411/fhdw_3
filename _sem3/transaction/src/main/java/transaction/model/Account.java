package transaction.model;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A simple account that possesses a list of account entries, the sum of which
 * constitutes the account's >balance>.
 */
public class Account {

	/**
	 * The balance of all accounts shall be greater or equal than this limit.
	 */
	public static final long UniversalAccountLimit = -1000;

	public static Account create(final String name) {
		return new Account(name);
	}

	private final String name;
	private long balance;
	List<Entry> accountEntries;
	private final List<AccountObserver> observers;

	public Account(final String name) {
		this.name = name;
		this.balance = 0;
		this.accountEntries = new LinkedList<>();
		this.observers = new LinkedList<>();
	}

	public long getBalance() {
		return this.balance;
	}

	public String getName() {
		return this.name;
	}

	public List<Entry> getAccountEntries() {
		return this.accountEntries;
	}

	public void register(final AccountObserver observer) {
		if (this.observers.contains(observer)) {
			return;
		}
		this.observers.add(observer);
	}

	public void deregister(final AccountObserver observer) {
		this.observers.remove(observer);
	}

	private void notifyObservers() {
		final Iterator<AccountObserver> currentObservers = this.observers.iterator();
		while (currentObservers.hasNext()) {
			currentObservers.next().update();
		}
	}

	public void book(final Entry entry) {
		entry.acceptEntryVisitor(new EntryVisitor<Void>() {

			@Override
			public Void handleDebitEntry(final DebitEntry debitEntry) {
				balance -= debitEntry.getAmount();
				accountEntries.add(debitEntry);
				return null;
			}

			@Override
			public Void handleCreditEntry(final CreditEntry creditEntry) {
				balance += creditEntry.getAmount();
				accountEntries.add(creditEntry);
				return null;
			}
		});
		this.notifyObservers();
	}

	@Override
	public String toString() {
		return this.name;
	}
}
