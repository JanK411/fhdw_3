package transaction.model;

/**
 * An entry (debit or credit) for an account, typically result of a booking
 * process.
 */
public abstract class Entry {

	protected final Transfer transfer;

	public long getAmount() {
		return transfer.getAmount();
	}

	public Entry(final Transfer transfer) {
		this.transfer = transfer;
	}

	abstract public <T> T acceptEntryVisitor(EntryVisitor<T> visitor);

}
