package transaction.viewModel;

public interface AccountViewManager {

	void handleAccountUpdate(AccountView accountView);

}
