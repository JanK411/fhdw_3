package transaction.viewModel;

import transaction.model.CreditEntry;
import transaction.model.DebitEntry;
import transaction.model.Entry;
import transaction.model.EntryVisitor;

public class EntryView {

	protected static final String DebitPrefix = "Debit: ";
	protected static final String CreditPrefix = "Credit: ";

	public static EntryView create(final Entry entry) {
		return new EntryView(entry);
	}

	private final Entry entry;

	public EntryView(final Entry entry) {
		this.entry = entry;
	}

	@Override
	public String toString() {
		// TODO Implement reasonable string representation!!!
		return this.entry.acceptEntryVisitor(new EntryVisitor<String>() {

			@Override
			public String handleDebitEntry(final DebitEntry debitEntry) {
				return debitEntry.toString();
			}

			@Override
			public String handleCreditEntry(final CreditEntry creditEntry) {
				// TODO Auto-generated method stub
				return creditEntry.toString();
			}
		});
	}

}
