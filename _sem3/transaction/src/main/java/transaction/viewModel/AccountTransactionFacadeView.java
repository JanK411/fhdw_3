package transaction.viewModel;

public interface AccountTransactionFacadeView {

	void updateEntriesOfSelectedAccount();

}
