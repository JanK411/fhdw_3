package transaction.viewModel;

import java.util.Enumeration;

import javax.swing.DefaultListModel;
import javax.swing.ListModel;

import transaction.model.Account;
import transaction.model.AccountException;
import transaction.model.AccountManager;

public class ViewModel implements AccountViewManager {

	public static ViewModel create(final AccountTransactionFacadeView view) {
		return new ViewModel(view);
	}

	private final SpecialDefaultListModel<AccountView> myAccounts;
	private final SpecialDefaultListModel<AccountView> otherAccounts;
	private AccountView selectedAccount;
	private ListModel<EntryView> currentEntries;
	private final DefaultListModel<TransferOrTransactionView> pendingTransfersAndOrTransactions;
	private TransactionView selectedTransaction;
	private DefaultListModel<TransferOrTransactionView> currentTransactionDetails;
	private final AccountTransactionFacadeView view;

	private ViewModel(final AccountTransactionFacadeView view) {
		this.view = view;
		this.myAccounts = new SpecialDefaultListModel<>();
		this.otherAccounts = new SpecialDefaultListModel<>();
		this.currentEntries = new DefaultListModel<>();
		this.pendingTransfersAndOrTransactions = new SpecialDefaultListModel<>();
		this.currentTransactionDetails = new SpecialDefaultListModel<>();
	}

	public ListModel<AccountView> getMyAccountList() {
		return this.myAccounts;
	}

	public ListModel<AccountView> getOtherAccountList() {
		return this.otherAccounts;
	}

	public ListModel<EntryView> getCurrentAccountEntries() {
		return this.currentEntries;
	}

	public ListModel<TransferOrTransactionView> getPendingTransfersAndorTransactions() {
		return this.pendingTransfersAndOrTransactions;
	}

	public void createAccount(final String name) throws AccountException {
		final Account newAccount = AccountManager.getTheAccountManager().create(name);
		this.myAccounts.addElement(AccountView.create(newAccount, this));
	}

	public void findAccount(final String name) throws AccountException {
		final Account foundAccount = AccountManager.getTheAccountManager().find(name);
		if (this.containsInOtherAccounts(foundAccount)) {
			throw new AccountAlreadyShownException(name);
		}
		this.otherAccounts.addElement(AccountView.create(foundAccount, this));
	}

	private boolean containsInOtherAccounts(final Account account) {
		final Enumeration<AccountView> otherAccountsEnumeration = this.otherAccounts.elements();
		while (otherAccountsEnumeration.hasMoreElements()) {
			final AccountView current = otherAccountsEnumeration.nextElement();
			if (current.isFor(account)) {
				return true;
			}
		}
		return false;
	}

	public void clearOtherAccounts() {
		final Enumeration<AccountView> otherAccountsEnumeration = this.otherAccounts.elements();
		while (otherAccountsEnumeration.hasMoreElements()) {
			final AccountView current = otherAccountsEnumeration.nextElement();
			current.release();
		}
		this.otherAccounts.clear();
	}

	public void changeAccountSelection(final AccountView selectedAccount) {
		this.selectedAccount = selectedAccount;
		this.currentEntries = this.selectedAccount.getAccountEntries();
		this.view.updateEntriesOfSelectedAccount();
	}

	public void changeTransactionSelection(final TransactionView selectedTransaction) {
		this.selectedTransaction = selectedTransaction;
		this.currentTransactionDetails = this.selectedTransaction.getDetails();
	}

	@Override
	public void handleAccountUpdate(final AccountView accountView) {
		int index = 0;
		final Enumeration<AccountView> myAccountsEnumeration = this.myAccounts.elements();
		while (myAccountsEnumeration.hasMoreElements()) {
			final AccountView current = myAccountsEnumeration.nextElement();
			if (current.equals(accountView)) {
				this.myAccounts.fireEntryChanged(index);
				break;
			}
			index++;
		}
		index = 0;
		final Enumeration<AccountView> otherAccountsEnumeration = this.otherAccounts.elements();
		while (otherAccountsEnumeration.hasMoreElements()) {
			final AccountView current = otherAccountsEnumeration.nextElement();
			if (current.equals(accountView)) {
				this.otherAccounts.fireEntryChanged(index);
				break;
			}
			index++;
		}
		if (this.selectedAccount != null && this.selectedAccount.equals(accountView)) {
			this.changeAccountSelection(this.selectedAccount);
		}
	}

	public void createTransfer(final AccountView from, final AccountView to, final long amount, final String purpose) {
		final TransferView newTransaction = TransferView.create(from, to, amount, purpose);
		this.pendingTransfersAndOrTransactions.addElement(newTransaction);
	}

	public void createTransferInTransaction(final AccountView from, final AccountView to, final long amount,
			final String purpose, final TransactionView transaction) {
		transaction.addTransfer(from, to, amount, purpose);
	}

	public void createTransaction() {
		final TransactionView newTransactionView = TransactionView.create();
		this.pendingTransfersAndOrTransactions.addElement(newTransactionView);
	}

	public ListModel<TransferOrTransactionView> getCurrentTransactionDetails() {
		return this.currentTransactionDetails;
	}

	public void book(final TransferOrTransactionView transferOrTransaction) throws AccountException {
		transferOrTransaction.getTransferOrTransaction().book();
		this.pendingTransfersAndOrTransactions.removeElement(transferOrTransaction);
	}
}
