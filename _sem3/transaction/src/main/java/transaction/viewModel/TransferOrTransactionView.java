package transaction.viewModel;

import transaction.model.TransferOrTransaction;

public interface TransferOrTransactionView {

	void accept(TransferOrTransactionViewVisitor visitor);

	TransferOrTransaction getTransferOrTransaction();

}
