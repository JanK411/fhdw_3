package transaction.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.Test;

public class TransferTest {

	private static Account acc1;
	private static Account acc2;

	@BeforeClass
	public static void setUp() {
		try {
			acc1 = AccountManager.getTheAccountManager().create("acc1");
			acc2 = AccountManager.getTheAccountManager().create("acc2");
		} catch (final AccountException e) {
			// macht nix
		}
	}

	@Test
	public void testTransfer() throws AccountNotFoundException {
		final long balance1 = acc1.getBalance();
		final long balance2 = acc2.getBalance();
		final int accountEntriesAmount1 = acc1.getAccountEntries().size();
		final int accountEntriesAmount2 = acc2.getAccountEntries().size();

		final Transfer transfer = Transfer.create(acc1, acc2, 42, "einfach nur ein Testfall!");
		transfer.book();

		assertEquals(balance1 - 42, acc1.getBalance());
		assertEquals(balance2 + 42, acc2.getBalance());

		assertEquals(accountEntriesAmount1 + 1, acc1.getAccountEntries().size());
		assertEquals(accountEntriesAmount2 + 1, acc2.getAccountEntries().size());
	}

	@Test
	public void testAccountException() {
		final AccountManager manager = AccountManager.getTheAccountManager();
		try {
			manager.create("acc1");
			manager.create("acc1");
			fail();
		} catch (final AccountException e) {
			// everything perfect!
		}

	}

	@Test
	public void transactions() throws AccountNotFoundException {
		final Transaction transaction = Transaction.create();

		final long oldBalance1 = acc1.getBalance();
		final long oldBalance2 = acc2.getBalance();
		final int accountEntriesAmount1 = acc1.getAccountEntries().size();
		final int accountEntriesAmount2 = acc2.getAccountEntries().size();

		transaction.addTransfer(Transfer.create(acc1, acc2, -5, "assdf"));
		/* negative amount */
		transaction.addTransfer(Transfer.create(acc1, acc2, 10, "assd54654f"));
		/* positive amount */
		transaction.addTransfer(Transfer.create(acc1, acc2, 0, "ohnealles"));
		/* zero amount */

		transaction.book();
		assertEquals(oldBalance1 - 15, acc1.getBalance());
		assertEquals(oldBalance2 + 15, acc2.getBalance());

		assertEquals(accountEntriesAmount1 + 3, acc1.getAccountEntries().size());
		assertEquals(accountEntriesAmount2 + 3, acc2.getAccountEntries().size());

	}

}
