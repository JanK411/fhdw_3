package diningPhilosophers.observer;

import diningPhilosophers.Philosopher;

public class PhilosopherStartsThinkingEvent extends PhilosopherEvent {

	public PhilosopherStartsThinkingEvent(final Philosopher philosopher) {
		super(philosopher);
	}

	@Override
	public void accept(final PhilosopherEventVisitor v) {
		v.handle(this);
	}

}
