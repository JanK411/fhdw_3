package diningPhilosophers.observer;

import diningPhilosophers.Philosopher;

public class PhilosopherTookLeftEvent extends PhilosopherEvent {

	public PhilosopherTookLeftEvent(Philosopher philosopher) {
		super(philosopher);
	}

	@Override
	public void accept(PhilosopherEventVisitor v) {
		v.handle(this);
	}

}
