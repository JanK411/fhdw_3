package diningPhilosophers.observer;

import diningPhilosophers.Philosopher;

public class PhilosopherTookRightEvent extends PhilosopherEvent {

	public PhilosopherTookRightEvent(Philosopher philosopher) {
		super(philosopher);
	}

	@Override
	public void accept(PhilosopherEventVisitor v) {
		v.handle(this);
	}

}
