package diningPhilosophers.observer;

public interface PhilosopherEventVisitor {

	void handle(PhilosopherStartsThinkingEvent philosopherStartsThinkingEvent);

	void handle(PhilosopherStartsEatingEvent philosopherStartsEatingEvent);

	void handle(PhilosopherTookRightEvent philosopherTookRightEvent);

	void handle(PhilosopherTookLeftEvent philosopherTookLeftEvent);

}
