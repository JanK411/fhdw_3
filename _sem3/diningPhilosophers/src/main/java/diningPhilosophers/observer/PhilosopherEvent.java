package diningPhilosophers.observer;

import diningPhilosophers.Philosopher;

public abstract class PhilosopherEvent {

	Philosopher philosopher;

	public PhilosopherEvent(Philosopher philosopher) {
		this.philosopher = philosopher;
	}

	public abstract void accept(PhilosopherEventVisitor v);

	public String getPhilosopher() {
		return this.philosopher.getName();
	}
}
