package diningPhilosophers.observer;

import diningPhilosophers.Philosopher;

public class PhilosopherStartsEatingEvent extends PhilosopherEvent {

	public PhilosopherStartsEatingEvent(final Philosopher philosopher) {
		super(philosopher);
	}

	@Override
	public void accept(final PhilosopherEventVisitor v) {
		v.handle(this);
	}

}
