package diningPhilosophers;

public class EBM {

	boolean isTaken = false;

	public void get() throws EBMException {
		if (isTaken)
			throw new EBMException("EBM already taken");
		isTaken = true;
	}

	public void put() throws EBMException {
		if (!isTaken)
			throw new EBMException("EBM can't be put if not taken");
		isTaken = false;
	}
}
