package diningPhilosophers;

public class EBMException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EBMException(String message) {
		super(message);
	}

}
