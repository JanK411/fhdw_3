package diningPhilosophers;

import java.util.ArrayList;
import java.util.Collection;

public class Starter {
	public static void main(final String[] args) throws InterruptedException {
		runPhilosophers(999999999, 15, true);

	}

	private static void runPhilosophers(final int numberOfPhilosophers, final int numberOfSeconds, final boolean careAboutEBM)
			throws InterruptedException {
		final Collection<Philosopher> philosophers = new ArrayList<>();

		for (int i = 0; i < numberOfPhilosophers; i++) {
			philosophers.add(new Philosopher(careAboutEBM));
		}

		PTOMonitor.getInstance().organizePhilosophers(philosophers);
		philosophers.forEach(p -> new Thread(p).start());

		final Object o = new Object();

		System.out.println("alala!");
		synchronized (o) {
			o.wait(numberOfSeconds * 1000);
		}
		System.out.println("lol");

		philosophers.forEach(p -> p.stop());
	}

}
