package diningPhilosophers;

import java.util.Random;

import diningPhilosophers.observer.PhilosopherStartsEatingEvent;
import diningPhilosophers.observer.PhilosopherStartsThinkingEvent;
import diningPhilosophers.observer.PhilosopherTookLeftEvent;
import diningPhilosophers.observer.PhilosopherTookRightEvent;

public class Philosopher implements Runnable {

	private static int nr = 0;

	private final boolean careAboutEBMMode;

	private boolean running = true;

	private final int number;

	private EBM left;
	private EBM right;

	public Philosopher(final boolean careAboutEBMMode) {
		this.careAboutEBMMode = careAboutEBMMode;
		this.number = nr;
		nr++;
	}

	@Override
	public void run() {
		System.out.println(this + " is started");
		while (this.running) {
			this.startEating();
			synchronized (this) {
				try {
					this.wait(this.randTimeout());
				} catch (final InterruptedException e) {
				}
			}
			this.startThinking();
			synchronized (this) {
				try {
					this.wait(this.randTimeout());
				} catch (final InterruptedException e) {
				}
			}
		}

	}

	private void startEating() {
		if (this.careAboutEBMMode) {
			synchronized (this) {
				if (this.number % 2 == 0) {
					this.takeLeft();
					this.takeRight();
				} else {
					this.takeRight();
					this.takeLeft();
				}
			}
		}
		PTOMonitor.getInstance().update(new PhilosopherStartsEatingEvent(this));
	}

	private void takeRight() {
		boolean gotRight = false;
		while (!gotRight) {
			try {
				this.right.get();
				gotRight = true;
			} catch (EBMException e) {
				try {
					this.wait();
				} catch (InterruptedException e1) {
					gotRight = true;
				}
			}
		}
		PTOMonitor.getInstance().update(new PhilosopherTookRightEvent(this));
	}

	private void takeLeft() {
		boolean gotLeft = false;
		while (!gotLeft) {
			try {
				this.left.get();
				gotLeft = true;
			} catch (EBMException e) {
				try {
					this.wait();
				} catch (InterruptedException e1) {
					gotLeft = true;
				}
			}
		}
		PTOMonitor.getInstance().update(new PhilosopherTookLeftEvent(this));
	}

	private void startThinking() {
		if (this.careAboutEBMMode) {
			try {
				this.left.put();
				this.right.put();
			} catch (EBMException e) {
				throw new Error(e);
			}
		}
		PTOMonitor.getInstance().update(new PhilosopherStartsThinkingEvent(this));
	}

	private long randTimeout() {
		return (long) (1000 * new Random().nextFloat() + 1);
	}

	public String getName() {
		return "Philosopher nr. " + this.number;
	}

	public void stop() {
		this.running = false;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public void setLeftMark(final EBM left) {
		this.left = left;
	}

	public void setRightMark(final EBM right) {
		this.right = right;
	}

	public void notifySomeoneFinished() {
		synchronized (this) {
			this.notify();
		}
	}
}
