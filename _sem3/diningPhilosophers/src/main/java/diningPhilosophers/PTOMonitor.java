package diningPhilosophers;

import java.util.Collection;
import java.util.Iterator;

import diningPhilosophers.observer.PhilosopherEvent;
import diningPhilosophers.observer.PhilosopherEventVisitor;
import diningPhilosophers.observer.PhilosopherStartsEatingEvent;
import diningPhilosophers.observer.PhilosopherStartsThinkingEvent;
import diningPhilosophers.observer.PhilosopherTookLeftEvent;
import diningPhilosophers.observer.PhilosopherTookRightEvent;

public class PTOMonitor {
	private static PTOMonitor instance;

	public static PTOMonitor getInstance() {
		if (instance == null) {
			instance = new PTOMonitor();
		}
		return instance;
	}

	private Collection<Philosopher> philosophers;

	private PTOMonitor() {
	}

	public void update(final PhilosopherEvent e) {
		e.accept(new PhilosopherEventVisitor() {

			@Override
			public void handle(final PhilosopherStartsThinkingEvent philosopherStartsThinkingEvent) {
				System.out.println(philosopherStartsThinkingEvent.getPhilosopher() + " beginnt zu denken");
				notifyAllPhilosophers();
			}

			@Override
			public void handle(final PhilosopherStartsEatingEvent philosopherStartsEatingEvent) {
				System.out.println(philosopherStartsEatingEvent.getPhilosopher() + " beginnt zu essen");
			}

			@Override
			public void handle(PhilosopherTookRightEvent philosopherTookRightEvent) {
				System.out.println(philosopherTookRightEvent.getPhilosopher() + " hat sich die rechte Marke genommen");
			}

			@Override
			public void handle(PhilosopherTookLeftEvent philosopherTookLeftEvent) {
				System.out.println(philosopherTookLeftEvent.getPhilosopher() + " hat sich die linke Marke genommen");
			}

		});
	}

	private void notifyAllPhilosophers() {
		this.philosophers.forEach(p -> p.notifySomeoneFinished());
	}

	/**
	 * setzt die Philosophen um den Tisch herum und verteild die EBMs
	 * 
	 * @param philosophers
	 */
	public void organizePhilosophers(final Collection<Philosopher> philosophers) {
		this.philosophers = philosophers;
		Iterator<Philosopher> philIterator = philosophers.iterator();
		Philosopher preNext;
		if (philIterator.hasNext())
			preNext = philIterator.next();
		else
			throw new Error();
		Philosopher first = preNext;

		while (philIterator.hasNext()) {
			Philosopher next = philIterator.next();
			EBM nextEBM = new EBM();

			preNext.setRightMark(nextEBM);
			next.setLeftMark(nextEBM);
			preNext = next;
		}
		EBM last = new EBM();
		preNext.setRightMark(last);
		first.setLeftMark(last);
	}

}
