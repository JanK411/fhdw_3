package concurrentSortingAlgorithms;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ MergeSortTest.class, InsertionSortTest.class, BubbleSortTest.class })
public class TestAll {

}
