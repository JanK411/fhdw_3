package concurrentSortingAlgorithms;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import concurrentSortingAlgorithms.bubblesort.BubbleSort;

public class BubbleSortTest {

	private LinkedList<Integer> sorted1_6;

	@Before
	public void setUp() throws Exception {
		sorted1_6 = new LinkedList<>();
		sorted1_6.add(1);
		sorted1_6.add(2);
		sorted1_6.add(3);
		sorted1_6.add(4);
		sorted1_6.add(5);
		sorted1_6.add(6);
	}

	@Test
	public void testUnsortedList() {
		final List<Integer> ints = new LinkedList<>();
		ints.add(6);
		ints.add(2);
		ints.add(1);
		ints.add(4);
		ints.add(3);
		ints.add(5);

		for (int i = 0; i < 100; i++) {
			assertEquals(sorted1_6, BubbleSort.sort(ints));
		}
	}

	@Test
	public void testBigList() {
		final List<Integer> unsorted = new ArrayList<>();
		final List<Integer> sorted = new ArrayList<>();
		IntStream.range(0, 1000).forEach(e -> {
			unsorted.add(e);
			sorted.add(e);
		});
		for (int i = 0; i < 5; i++) {
			Collections.shuffle(unsorted);
			assertEquals(sorted, BubbleSort.sort(unsorted));
		}
	}

	@Test
	public void testWithDoubleEntries() {
		final List<Integer> unsorted = new ArrayList<>();
		final List<Integer> sorted = new ArrayList<>();
		IntStream.range(0, 5).forEach(e -> {
			IntStream.range(0, 5).forEach(f -> {
				unsorted.add(f);
				sorted.add(f);
			});
		});
		Collections.sort(sorted);
		for (int i = 0; i < 10; i++) {
			Collections.shuffle(unsorted);
			assertEquals(sorted, BubbleSort.sort(unsorted));
		}
	}

	@Test
	public void testAlreadySortedList() {
		assertEquals(sorted1_6, BubbleSort.sort(sorted1_6));
	}

	@Test
	public void oneElementList() {
		final List<Integer> unsorted = new ArrayList<>();
		unsorted.add(42);
		assertEquals(unsorted, BubbleSort.sort(unsorted));
	}

	@Test
	public void listWithTwoElements() {
		final List<Integer> unsorted = new ArrayList<>();
		final List<Integer> sorted = new ArrayList<>();

		unsorted.add(42);
		unsorted.add(23);
		sorted.add(23);
		sorted.add(42);

		assertEquals(sorted, BubbleSort.sort(sorted));
		assertEquals(sorted, BubbleSort.sort(unsorted));
	}

	@Test
	public void listWithOnlySameElements() {
		final List<Integer> unsorted = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			unsorted.add(42);
		}
		assertEquals(unsorted, BubbleSort.sort(unsorted));
	}

	@Test
	public void testEmptyList() {
		final List<Integer> unsorted = new ArrayList<>();
		assertEquals(new ArrayList<>(), BubbleSort.sort(unsorted));
	}
}
