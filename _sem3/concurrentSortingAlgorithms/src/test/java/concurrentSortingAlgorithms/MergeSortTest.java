package concurrentSortingAlgorithms;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import concurrentSortingAlgorithms.mergesort.MergeSort;

public class MergeSortTest {

	private LinkedList<Integer> sorted1_6;

	@Before
	public void setUp() throws Exception {
		sorted1_6 = new LinkedList<>();
		sorted1_6.add(1);
		sorted1_6.add(2);
		sorted1_6.add(3);
		sorted1_6.add(4);
		sorted1_6.add(5);
		sorted1_6.add(6);
	}

	@Test
	public void testUnsortedList() {
		final List<Integer> ints = new LinkedList<>();
		ints.add(6);
		ints.add(2);
		ints.add(1);
		ints.add(4);
		ints.add(3);
		ints.add(5);

		for (int i = 0; i < 100; i++)
			assertEquals(sorted1_6, MergeSort.sort(ints));
	}

	@Test
	public void testBigList() {
		final List<Integer> unsorted = new ArrayList<>();
		List<Integer> sorted = new ArrayList<>();
		IntStream.range(0, 1000).forEach(e -> {
			unsorted.add(e);
			sorted.add(e);
		});
		for (int i = 0; i < 5; i++) {
			Collections.shuffle(unsorted);
			assertEquals(sorted, MergeSort.sort(unsorted));
		}
	}

	@Test
	public void testWithDoubleEntries() {
		final List<Integer> unsorted = new ArrayList<>();
		List<Integer> sorted = new ArrayList<>();
		IntStream.range(0, 5).forEach(e -> {
			IntStream.range(0, 5).forEach(f -> {
				unsorted.add(f);
				sorted.add(f);
			});
		});
		Collections.sort(sorted);
		for (int i = 0; i < 10; i++) {
			Collections.shuffle(unsorted);
			assertEquals(sorted, MergeSort.sort(unsorted));
		}
	}

	@Test
	public void testAlreadySortedList() {
		assertEquals(sorted1_6, MergeSort.sort(sorted1_6));
	}

	@Test
	public void testEmptyList() {
		List<Integer> unsorted = new ArrayList<>();
		assertEquals(new ArrayList<>(), MergeSort.sort(unsorted));
	}

	@Test
	public void oneElelelelementList() {
		List<Integer> unsorted = new ArrayList<>();
		unsorted.add(42);
		assertEquals(unsorted, MergeSort.sort(unsorted));
	}

	@Test
	public void listWithTwoElements() {
		List<Integer> unsorted = new ArrayList<>();
		List<Integer> sorted = new ArrayList<>();

		unsorted.add(42);
		unsorted.add(23);
		sorted.add(23);
		sorted.add(42);

		assertEquals(sorted, MergeSort.sort(sorted));
		assertEquals(sorted, MergeSort.sort(unsorted));
	}

	@Test
	public void listWithOnlySameElements() {
		List<Integer> unsorted = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			unsorted.add(42);
		}
		assertEquals(unsorted, MergeSort.sort(unsorted));
	}
}
