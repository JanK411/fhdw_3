package concurrentSortingAlgorithms.bubblesort;

import java.util.ArrayList;
import java.util.List;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.ErrorElementException;

public class BubbleSort {

	private boolean sortingFinished = false;

	private final List<OneTimeBubbler> listOfStartedThreads;

	public static List<Integer> sort(final List<Integer> list) throws ErrorElementException {
		final Buffer<Integer> buffer = new Buffer<>(list);
		return sort(buffer).toList();
	}

	public BubbleSort() {
		listOfStartedThreads = new ArrayList<>();
	}

	private static Buffer<Integer> sort(final Buffer<Integer> buffer) {
		if (buffer.size() <= 2)
			return buffer;

		final BubbleSort bubbleSort = new BubbleSort();
		final OneTimeBubbler bubbleSortOneTimeThing = new OneTimeBubbler(buffer, bubbleSort);
		bubbleSort.listOfStartedThreads.add(bubbleSortOneTimeThing);
		new Thread(() -> bubbleSortOneTimeThing.startSorting()).start();
		synchronized (bubbleSort) {
			while (!bubbleSort.sortingFinished) {
				try {
					bubbleSort.wait();
				} catch (final InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bubbleSort.listOfStartedThreads.get(0).getOutputBuffer();
	}

	public synchronized void startNewThread(final Buffer<Integer> outputBuffer) {
		final OneTimeBubbler bubbleSortOneTimeThing = new OneTimeBubbler(outputBuffer, this);
		listOfStartedThreads.add(bubbleSortOneTimeThing);
		new Thread(() -> bubbleSortOneTimeThing.startSorting()).start();
	}

	public synchronized void oneTimeSortFinished(final OneTimeBubbler sorter) {
		if (listOfStartedThreads.size() <= 1) {
			sortingFinished = true;
			this.notify();
		} else {
			listOfStartedThreads.remove(sorter);
		}
	}
}