package concurrentSortingAlgorithms.bubblesort;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.StoppException;

public class OneTimeBubbler {

	private final Buffer<Integer> inputBuffer;
	private final Buffer<Integer> outputBuffer;
	private boolean activatedNext;
	private final BubbleSort bubbleSort;

	public OneTimeBubbler(final Buffer<Integer> buffer, final BubbleSort bubbleSort) {
		this.inputBuffer = buffer;
		this.outputBuffer = new Buffer<>(buffer.getCapacity());
		this.activatedNext = false;
		this.bubbleSort = bubbleSort;
	}

	public void startSorting() {
		boolean running = true;

		Integer first = null;
		Integer second = null;

		try {
			first = inputBuffer.get();
		} catch (final StoppException e) {
			e.printStackTrace();
			// sollte niemals passieren, wenn die Liste mehr als 0 elemente
			// hat!
		}
		while (running) {
			try {
				second = inputBuffer.get();
				if (first.compareTo(second) < 1) {
					getOutputBuffer().put(first);
					first = second;
				} else {
					getOutputBuffer().put(second);
					if (activatedNext == false) {
						bubbleSort.startNewThread(getOutputBuffer());
						activatedNext = true;
					}
				}
			} catch (final StoppException e) {
				getOutputBuffer().put(first);
				getOutputBuffer().stopp();
				running = false;
			}
		}
		bubbleSort.oneTimeSortFinished(this);
	}

	public Buffer<Integer> getOutputBuffer() {
		return outputBuffer;
	}

}
