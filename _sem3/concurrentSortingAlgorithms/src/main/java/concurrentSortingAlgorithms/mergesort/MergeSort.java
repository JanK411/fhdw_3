package concurrentSortingAlgorithms.mergesort;

import java.util.List;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.ErrorElementException;
import lockAndBuffer.Buffer.StoppException;

public class MergeSort {

	public static List<Integer> sort(final List<Integer> ints) {
		Buffer<Integer> buffer = new Buffer<Integer>(ints);
		MergeSort MSN = new MergeSort();
		return MSN.sort(buffer).toList();
	}

	private Buffer<Integer> sort(final Buffer<Integer> buffer) {
		Splitter splitter = new Splitter(buffer);
		if (buffer.size() > 2) {
			new Thread(() -> splitter.startSplitting()).start();
		} else {
			return buffer;
		}

		splitter.lockTheLock();

		return merge(sort(splitter.getLeftBuffer()), sort(splitter.getRightBuffer()));

	}

	private Buffer<Integer> merge(Buffer<Integer> leftBuffer, Buffer<Integer> rightBuffer) {
		Buffer<Integer> ret = new Buffer<>(leftBuffer.getCapacity() + rightBuffer.getCapacity() - 1);

		Integer leftOutput = null;
		Integer rightOutput = null;
		try {
			leftOutput = leftBuffer.get();
			rightOutput = rightBuffer.get();
		} catch (StoppException | ErrorElementException e) {
			throw new Error("Schwachkopf!! <-- tritt ehh nie auf :3", e);
		}

		boolean merging = true;
		while (merging) {
			if (leftOutput.compareTo(rightOutput) > 0) {
				ret.put(rightOutput);
				try {
					rightOutput = rightBuffer.get();
				} catch (StoppException e) {
					ret.put(leftOutput);
					ret.fill(leftBuffer);
					ret.stopp();
					merging = false;
				} catch (ErrorElementException e) {
					throw new Error(e);
				}
			} else {
				ret.put(leftOutput);
				try {
					leftOutput = leftBuffer.get();
				} catch (StoppException e) {
					ret.put(rightOutput);
					ret.fill(rightBuffer);
					ret.stopp();
					merging = false;
				} catch (ErrorElementException e) {
					throw new Error(e);
				}
			}
		}

		return ret;
	}
}