package concurrentSortingAlgorithms.mergesort;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.ErrorElementException;
import lockAndBuffer.Buffer.StoppException;
import lockAndBuffer.Lock;

public class Splitter {

	Lock lock = new Lock(true);
	private final Buffer<Integer> inputBuffer;
	private Buffer<Integer> leftOutputBuffer;
	private Buffer<Integer> rightOutputBuffer;

	public Splitter(Buffer<Integer> buffer) {
		this.inputBuffer = buffer;
		this.leftOutputBuffer = new Buffer<>((inputBuffer.getCapacity() / 2) + 2);
		this.rightOutputBuffer = new Buffer<>((inputBuffer.getCapacity() / 2) + 1);
	}

	public void startSplitting() {
		while (true) {
			try {
				leftOutputBuffer.put(inputBuffer.get());
				rightOutputBuffer.put(inputBuffer.get());
			} catch (StoppException e) {
				leftOutputBuffer.stopp();
				rightOutputBuffer.stopp();
				this.lock.unlock();
				break;
			} catch (ErrorElementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Buffer<Integer> getLeftBuffer() {
		return this.leftOutputBuffer;
	}

	public Buffer<Integer> getRightBuffer() {
		return this.rightOutputBuffer;
	}

	public void lockTheLock() {
		this.lock.lock();
	}

}
