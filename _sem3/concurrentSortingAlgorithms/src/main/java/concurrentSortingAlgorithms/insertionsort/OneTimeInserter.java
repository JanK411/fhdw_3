package concurrentSortingAlgorithms.insertionsort;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.StoppException;

public class OneTimeInserter<T extends Comparable<T>> {
	private final T element;
	private final Buffer<T> alreadySorted;
	private final Buffer<T> outputBuffer;

	public OneTimeInserter(final T element, final Buffer<T> alreadySorted) {
		this.element = element;
		this.alreadySorted = alreadySorted;
		this.outputBuffer = new Buffer<>(alreadySorted.getCapacity() + 1);
	}

	public void sortInto() {
		/*
		 * also possible with a boolean variable that gets set to false instead
		 * of break statement in catch-block
		 */
		while (true) {
			T next = null;
			try {
				next = alreadySorted.get();
			} catch (final StoppException e) {
				/*
				 * alreadySorted is empty and element gets puttet as last
				 * element
				 */ outputBuffer.put(element);
				outputBuffer.stopp();
				break;
			}

			if (element.compareTo(next) <= 0) {
				/*
				 * element is smaller than last element of alreadySorted list,
				 * so rest can be puttet
				 */
				outputBuffer.put(element);
				outputBuffer.put(next);
				outputBuffer.fill(alreadySorted);
				break;
			} else {
				outputBuffer.put(next);
			}
		}
	}

	public Buffer<T> getOutputBuffer() {
		return this.outputBuffer;
	}

}
