package concurrentSortingAlgorithms.insertionsort;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import lockAndBuffer.Buffer;
import lockAndBuffer.Buffer.StoppException;

public class InsertionSort<T extends Comparable<T>> {
	private Buffer<T> unsorted;
	Queue<OneTimeInserter<T>> inserterList = new LinkedList<>();

	/**
	 * @param toBeSorted
	 *            unsorted list
	 * @return sorted List
	 */
	public static <E extends Comparable<E>> List<E> sort(final List<E> toBeSorted) {
		final InsertionSort<E> insertionSort = new InsertionSort<E>();
		insertionSort.unsorted = new Buffer<E>(toBeSorted);
		return insertionSort.sort().toList();
	}

	/**
	 * @return the sorted version of the field {@code unsorted}
	 */
	private Buffer<T> sort() {
		if (unsorted.size() <= 2)
			return unsorted;

		OneTimeInserter<T> currentLast;
		try {
			currentLast = new OneTimeInserter<T>(unsorted.get(), new Buffer<T>(1).stopp());
			inserterList.add(currentLast);
		} catch (final StoppException e) {
			throw new Error(e);
			// should never happen, is tested some lines above
		}

		while (true) {
			/*
			 * also possible with a boolean variable that gets set to false
			 * instead of break statement in catch-block
			 */
			try {
				currentLast = new OneTimeInserter<T>(unsorted.get(), currentLast.getOutputBuffer());
				inserterList.add(currentLast);
			} catch (final StoppException e1) {
				break;
			}
		}

		inserterList.parallelStream().forEach(e -> new Thread(() -> e.sortInto()).start());

		return currentLast.getOutputBuffer();
	}

}
