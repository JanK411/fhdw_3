package concurrentPartsList.commands;

import concurrentPartsList.Component;
import concurrentPartsList.MaterialList;

public class GetMaterialListCommand implements Command {

	private final Component c;
	private MaterialList result;

	public GetMaterialListCommand(final Component c) {
		this.c = c;
	}

	@Override
	synchronized public void execute() {
		this.result = this.c.getRealMaterialList();
		this.notify();
	}

	@Override
	synchronized public MaterialList getResult() {
		while (this.result == null) {
			try {
				this.wait();
			} catch (final InterruptedException e) {
				break;
			}
		}
		return this.result;
	}

}
