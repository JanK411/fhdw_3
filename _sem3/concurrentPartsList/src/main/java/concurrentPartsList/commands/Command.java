package concurrentPartsList.commands;

public interface Command {
	void execute();

	Object getResult() throws Throwable;
}
