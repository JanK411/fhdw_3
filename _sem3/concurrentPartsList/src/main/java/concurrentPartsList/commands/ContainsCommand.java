package concurrentPartsList.commands;

import concurrentPartsList.Component;

public class ContainsCommand implements Command {
	private final Component c;
	private final Component d;
	private Boolean result;

	public ContainsCommand(final Component c, final Component d) {
		this.c = c;
		this.d = d;
	}

	@Override
	synchronized public void execute() {
		this.result = this.c.theRealContains(this.d);
		this.notify();
	}

	@Override
	synchronized public Boolean getResult() {
		while (this.result == null) {
			try {
				this.wait();
			} catch (final InterruptedException e) {
				break;
			}
		}
		return this.result;
	}

}
