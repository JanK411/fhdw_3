package concurrentPartsList;

import java.util.Collection;
import java.util.LinkedList;

import util.exceptions.CycleException;

public class Product extends Component {
	Collection<QuantifiedComponent> parts;

	public Product(final String name, final int price) {
		super(name, price);
		this.parts = new LinkedList<>();
	}

	@Override
	public boolean theRealContains(final Component c) {
		if (this.equals(c))
			return true;

		for (final QuantifiedComponent current : this.parts) {
			if (current.getComponent().contains(c))
				return true;
		}
		return false;
	}

	private void addPart(final QuantifiedComponent c) {
		this.parts.add(c);
	}

	void addPart(final Component c, final int quantity) {
		if (c.contains(this))
			throw new CycleException(c + " already contains " + this);
		this.addPart(new QuantifiedComponent(c, quantity));
	}

	@Override
	public int getOverallPrice() {
		int ret = this.getPrice();
		for (final QuantifiedComponent current : this.parts) {
			ret += current.getOverallPrice();
		}
		return ret;
	}

	@Override
	public MaterialList getRealMaterialList() {
		final MaterialList ret = new MaterialList();
		this.parts.forEach(p -> ret.add(p.getMaterialList(), 1));

		return ret;
	}
}
