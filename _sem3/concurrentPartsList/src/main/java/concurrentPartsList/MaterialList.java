package concurrentPartsList;

import java.util.LinkedHashMap;
import java.util.Map;

public class MaterialList {
	private final Map<Material, Integer> data;

	MaterialList() {
		this.data = new LinkedHashMap<>();
	}

	public static MaterialList create() {
		return new MaterialList();
	}

	public void add(final Material material, final Integer i) {
		final Integer amount = this.data.containsKey(material) ? this.data.get(material) + i : i;
		this.data.put(material, amount);
	}

	public void add(final MaterialList materialList, final int amount) {
		for (final Material current : materialList.data.keySet()) {
			this.add(current, amount * materialList.data.get(current));
		}
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof MaterialList) {
			final MaterialList toCompare = (MaterialList) obj;
			return toCompare.data.equals(this.data);
		} else
			return false;
	}
}
