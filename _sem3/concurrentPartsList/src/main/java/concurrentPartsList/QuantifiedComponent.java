package concurrentPartsList;

public class QuantifiedComponent {
	private final Component component;
	private final int quantity;

	public QuantifiedComponent(final Component component, final int quantity) {
		this.component = component;
		this.quantity = quantity;
	}

	public Component getComponent() {
		return this.component;
	}

	public int getOverallPrice() {
		return this.quantity * this.component.getOverallPrice();
	}

	@Override
	public String toString() {
		return this.quantity + " x " + this.component.toString();
	}

	public MaterialList getMaterialList() {
		final MaterialList ret = new MaterialList();
		ret.add(this.component.getRealMaterialList(), this.quantity);
		return ret;
	}

}
