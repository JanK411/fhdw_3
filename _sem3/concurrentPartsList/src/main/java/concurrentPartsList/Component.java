package concurrentPartsList;

import concurrentPartsList.commands.ContainsCommand;
import concurrentPartsList.commands.GetMaterialListCommand;

public abstract class Component {

	private final String name;
	private int price;

	public abstract boolean theRealContains(Component c);

	public boolean contains(final Component c) {
		final ContainsCommand command = new ContainsCommand(c, this);
		new Thread(() -> command.execute()).start();
		return command.getResult();
	}

	abstract public int getOverallPrice();

	public Component(final String name, final int price) {
		this.name = name;
		this.price = price;
	}

	public int getPrice() {
		return this.price;
	}

	public void changePrice(final int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public abstract MaterialList getRealMaterialList();

	public MaterialList getMaterialList() {
		final GetMaterialListCommand gmlc = new GetMaterialListCommand(this);
		new Thread(() -> gmlc.execute()).start();
		return gmlc.getResult();
	}

}
