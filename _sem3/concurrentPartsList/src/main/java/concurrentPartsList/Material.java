package concurrentPartsList;

public class Material extends Component {

	@Override
	public boolean theRealContains(final Component c) {
		return this.equals(c);
	}

	public Material(final String name, final int price) {
		super(name, price);
	}

	@Override
	public int getOverallPrice() {
		return this.getPrice();
	}

	@Override
	public MaterialList getRealMaterialList() {
		final MaterialList ret = new MaterialList();
		ret.add(this, 1);
		return ret;
	}

}
