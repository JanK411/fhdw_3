package concurrentPartsList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import util.exceptions.CycleException;

public class PartsListTest {

	Material nudel;
	Material gemuese;
	Material fleisch;
	Material ei;
	Material wasser;
	Material soya;

	Product sosse;
	Product ramen;

	@Before
	public void setUp() throws Exception {
		this.nudel = new Material("Nudel", 5);
		this.gemuese = new Material("Gemuese", 5);
		this.fleisch = new Material("Fleisch", 5);
		this.ei = new Material("Ei", 5);
		this.wasser = new Material("Wasser", 0);
		this.soya = new Material("Soya", 1);

		this.sosse = new Product("Sosse", 5);
		this.ramen = new Product("Ramen", 1);

		this.ramen.addPart(this.nudel, 5);
		this.ramen.addPart(this.sosse, 2);
		this.ramen.addPart(this.gemuese, 3);
		this.ramen.addPart(this.fleisch, 4);
		this.ramen.addPart(this.ei, 1);

		this.sosse.addPart(this.wasser, 3);
		this.sosse.addPart(this.soya, 2);
		this.sosse.addPart(this.gemuese, 1);
	}

	@Test
	public void testOverallPrice() {
		final Product test1 = new Product("test1", 1);
		final Material mat1 = new Material("mat1", 1);
		final Material mat2 = new Material("mat2", 4);

		test1.addPart(mat1, 1);
		test1.addPart(mat2, 3);

		assertEquals(14, test1.getOverallPrice());
	}

	@Test
	public void observerTest() {
		assertEquals(90, this.ramen.getOverallPrice());
		this.soya.changePrice(3);
		assertEquals(98, this.ramen.getOverallPrice());
	}

	@Test
	public void testCycleException() {
		try {
			this.ramen.addPart(this.ramen, 1);
			fail();
		} catch (final CycleException e) {
		}
	}

	@Test
	public void testCycleException_2() {
		try {
			final Product cycledings = new Product("cycledings", 2);
			cycledings.addPart(this.ramen, 2);
			this.ramen.addPart(cycledings, 1);
			fail();
		} catch (final CycleException e) {
		}
	}

	@Test
	public void testGetMaterialList() {
		final MaterialList list = new MaterialList();
		list.add(this.nudel, 5);
		list.add(this.fleisch, 4);
		list.add(this.ei, 1);
		list.add(this.wasser, 6);
		list.add(this.soya, 4);
		list.add(this.gemuese, 5);

		assertEquals(list, this.ramen.getMaterialList());
	}

}
