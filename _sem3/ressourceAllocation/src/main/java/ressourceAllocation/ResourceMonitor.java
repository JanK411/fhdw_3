package ressourceAllocation;

import lockAndBuffer.Semaphore;

public class ResourceMonitor {

	Semaphore readingSem = new Semaphore(3);
	Semaphore writingSem = new Semaphore(1);

	private int currentlyReading = 0;
	private int currentlyWriting = 0;

	public void enterForReading() {
		if (currentlyWriting > 0) {
			// hier muss gewartet werden
		} else {
			this.readingSem.down();
			currentlyReading++;
		}
	}

	public void enterForWriting() {
		if (currentlyReading > 0) {
			// hier muss gewartet werden
		} else {
			this.writingSem.down();
			currentlyWriting++;
		}
	}

	public void leaveFromReading() {
		readingSem.up();
		currentlyReading--;
	}

	public void leaveFromWriting() {
		writingSem.up();
		currentlyWriting--;
	}

}
