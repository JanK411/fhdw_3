package util.exceptions;

public class CycleException extends RuntimeException {
	
	public CycleException(final String string) {
		super(string);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2400740596677846068L;
	
}
