package lockAndBuffer;

public class Semaphore implements AbstractSemaphore {

	int count;

	public Semaphore(final int count) {
		this.count = count;
	}

	@Override
	public synchronized void down() {
		if (this.count == 0) {
		}
		while (this.count <= 0) {
			try {
				this.wait();
			} catch (final InterruptedException e) {
			}
		}
		this.count = this.count - 1;
	}

	@Override
	public synchronized void up() {
		this.count = this.count + 1;
		this.notify();
	}

}
