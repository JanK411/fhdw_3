package lockAndBuffer;

import java.util.ArrayList;
import java.util.Stack;

public class InfiniteThreadRunner {

	private final Semaphore limitingSem;
	private final Semaphore waitingSem;
	private final ArrayList<Terminatable> runningThreads;
	private final Stack<Terminatable> toBeRunnedThreads;
	private final Lock mutex = new Lock(false);
	private boolean running;

	public InfiniteThreadRunner(final int maxRunningThreads) {
		this.limitingSem = new Semaphore(maxRunningThreads);
		this.waitingSem = new Semaphore(0);
		this.runningThreads = new ArrayList<>();
		this.toBeRunnedThreads = new Stack<>();
	}

	public void start() {
		this.running = true;
		new Thread(() -> {
			while (this.running) {
				this.waitingSem.down();
				this.limitingSem.down();
				this.mutex.lock();
				if (!this.running) {
					break;
				}
				final Terminatable terminatable = this.toBeRunnedThreads.pop();
				this.runningThreads.add(terminatable);
				this.mutex.unlock();
				new Thread(terminatable).start();
			}
		}).start();
	}

	public void add(final Terminatable terminatable) {
		this.mutex.lock();
		this.toBeRunnedThreads.add(terminatable);
		this.waitingSem.up();
		this.mutex.unlock();
	}

	public void notifyThreadFinished(final Terminatable terminatable) {
		this.mutex.lock();
		this.runningThreads.remove(terminatable);
		this.limitingSem.up();
		this.mutex.unlock();
	}

	public boolean hasSomethingLeft() {
		this.mutex.lock();
		final boolean b = !(this.runningThreads.isEmpty() && this.toBeRunnedThreads.isEmpty());
		this.mutex.unlock();
		return b;
	}

	public void terminateEverythingSoftly() {
		this.mutex.lock();
		this.toBeRunnedThreads.clear();
		this.running = false;
		this.runningThreads.forEach(t -> t.terminateSoftly());
		this.mutex.unlock();
	}
}
