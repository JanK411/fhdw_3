package lockAndBuffer;

public class Lock implements AbstractLock {

	private boolean locked;
	/**Creates a new Lock with the lock-flag set to the
	 * value of the parameter. */ 
	public Lock(final boolean locked){
		this.locked = locked;
	}
	@Override
	synchronized public void lock(){
		while (this.locked) {
			try {
				this.wait();
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.locked = true;
	}
	@Override
	synchronized public void unlock(){
		this.locked = false;
		this.notify();
	}	
}
