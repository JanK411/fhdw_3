
package lockAndBuffer;

import java.util.ArrayList;
import java.util.List;

public class Buffer<E> {
	private final Object[] myBuffer;
	private int first;
	private int behindLast;
	private final int internalCapacity;
	private final Lock mutex;
	private final AbstractSemaphore readyForGet;
	private final AbstractSemaphore readyForPut;

	public static <E> Buffer<E> create(final int capacity) {
		return new Buffer<>(capacity);
	}

	@SuppressWarnings("serial")
	public static class StoppException extends Exception {
	}

	private abstract class BufferEntry<T> {
		abstract T getWrapped() throws StoppException, ErrorElementException;
	}

	@SuppressWarnings("serial")
	public static class ErrorElementException extends RuntimeException {

		public ErrorElementException(final String message) {
			super(message);
		}

	}

	private class ErrorElement<T> extends BufferEntry<T> {

		private final String message;

		public ErrorElement(final String message) {
			this.message = message;
		}

		@Override
		T getWrapped() throws StoppException, ErrorElementException {
			throw new ErrorElementException(this.message);
		}

	}

	private class Stopp<T> extends BufferEntry<T> {
		@Override
		public String toString() {
			return "Stoppelement!";
		}

		Stopp() {
		}

		@Override
		T getWrapped() throws StoppException {
			throw new StoppException();
		}
	}

	private class Wrapped<T> extends BufferEntry<T> {
		final private T wrapped;

		@Override
		public String toString() {
			return "Entry: " + this.wrapped.toString();
		}

		Wrapped(final T toBeWrapped) {
			this.wrapped = toBeWrapped;
		}

		@Override
		public T getWrapped() {
			return this.wrapped;
		}
	}

	public Buffer(final int capacity) {
		this.internalCapacity = (capacity <= 0 ? 1 : capacity) + 1;
		this.myBuffer = new Object[this.internalCapacity];
		this.first = 0;
		this.behindLast = 0;
		this.mutex = new Lock(false);
		this.readyForGet = new Semaphore(0);
		this.readyForPut = new Semaphore(capacity);
	}

	public Buffer(final List<E> list) {
		this(list.size() + 1);
		list.forEach(e -> this.put(e));
		this.stopp();
	}

	public Buffer<E> put(final E value) {
		return this.put(new Wrapped<>(value));

	}

	private Buffer<E> put(final BufferEntry<E> value) {
		this.readyForPut.down();
		this.mutex.lock();
		this.addNextEntry(value);
		this.mutex.unlock();
		this.readyForGet.up();
		return this;
	}

	public E get() throws StoppException {
		this.readyForGet.down();
		this.mutex.lock();
		final BufferEntry<E> result = this.getNextEntry();
		this.mutex.unlock();
		this.readyForPut.up();
		return result.getWrapped();
	}

	private void addNextEntry(final Buffer<E>.BufferEntry<E> wrapped) {
		this.myBuffer[this.behindLast] = wrapped;
		this.behindLast = (this.behindLast + 1) % this.internalCapacity;
	}

	private BufferEntry<E> getNextEntry() {
		@SuppressWarnings("unchecked")
		final BufferEntry<E> result = (BufferEntry<E>) this.myBuffer[this.first];
		this.first = (this.first + 1) % this.internalCapacity;
		return result;
	}

	public Buffer<E> stopp() {
		this.put(new Stopp<E>());
		return this;
	}

	/**
	 * @return die Anzahl der Elemente, die sich zzt. im Buffer befinden
	 */
	public int size() {
		this.mutex.lock();
		final int result = this.first <= this.behindLast ? this.behindLast - this.first
				: this.behindLast + this.internalCapacity - this.first;
		this.mutex.unlock();
		return result;
	}

	public int getCapacity() {
		return this.internalCapacity - 1;
	}

	public String asList() {
		String ret = "";
		for (final Object object : this.myBuffer) {
			ret += object + " | ";
		}
		return ret;
	}

	public List<E> toList() {
		final List<E> ret = new ArrayList<>();
		while (true) {
			try {
				ret.add(this.get());
				// TODO vielleicht etwas anderes mit dem ErrorElement machen?
			} catch (final StoppException e) {
				break;
			}
		}
		return ret;
	}

	public void putErrorElement(final String message) {
		this.put(new ErrorElement<E>(message));
	}

	/**
	 * füllt {@code this} mit den Elementen von {@code inputBuffer}. Ein
	 * mögliches StoppElement in {@code this} wird nicht entfernt!!
	 *
	 * @param inputBuffer
	 */
	public void fill(final Buffer<E> inputBuffer) {
		while (true) {
			try {
				this.put(inputBuffer.get());
			} catch (final StoppException e) {
				this.stopp();
				break;
			} catch (final ErrorElementException e) {
				this.putErrorElement(e.getMessage());
			}
		}
		;
	}

	@Override
	public String toString() {
		String ret = "[";
		for (final Object current : this.myBuffer) {
			ret += current + "; ";
		}
		return ret + "]";
	}

	public boolean isEmpty() {
		return this.size() == 0;
	}
}
