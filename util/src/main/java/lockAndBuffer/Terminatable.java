package lockAndBuffer;

public interface Terminatable extends Runnable {
	public void terminateSoftly();
}
