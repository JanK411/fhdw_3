package lockAndBuffer;

public class ConstantBuffer<E> extends Buffer<E> {
	private final E constant;
	private boolean isStopped = false;

	public ConstantBuffer(final E constant, final int capacity) {
		super(capacity);
		this.constant = constant;
	}

	@Override
	public E get() throws StoppException {
		if (this.isStopped)
			throw new StoppException();
		return this.constant;
	}

	@Override
	public ConstantBuffer<E> stopp() {
		this.isStopped = true;
		return this;
	}

}
