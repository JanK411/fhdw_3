package partslist.model;

public class UnknownComponentException extends RuntimeException {
	public UnknownComponentException(final String string) {
		super(string);
	}
	
	/** * */
	private static final long serialVersionUID = -3045043839372813521L;
}
