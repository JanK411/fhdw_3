package automaton;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class State {
	
	private final Automaton automaton;
	
	private final String name;
	
	public static int i = 0;
	
	private State(final Automaton automaton) {
		this.automaton = automaton;
		this.name = "(z" + i + ")";
		i++;
	}
	
	public static State create(final Automaton aut) {
		final State state = new State(aut);
		aut.addStateToStateSet(state);
		return state;
	}
	
	void add(final InputParameter c, final State s) {
		this.getAutomaton().addTransition(Transition.create(this, c, s));
	}
	
	public void add(final char c, final State s) {
		this.add(InputCharacter.create(c), s);
	}
	
	/**
	 * 
	 * Liefert alle möglichen Zustände welche mit {@code c} aus {@code this}
	 * erreicht werden.
	 * 
	 * @param c
	 *            eingegebener Buchstabe
	 * @return StateSet
	 */
	public StateSet get(final InputParameter c) {
		return this.get(c, new LinkedList<>());
	}
	
	/**
	 * @param c
	 *            inputparameter
	 * @param previouslyVisitiedTransitions
	 *            List of previously visited transitions
	 *            {@see Kleider-Bürsten-Prinzip}
	 * @return StateSet
	 * 
	 */
	private StateSet get(final InputParameter c, final List<Transition> previouslyVisitiedTransitions) {
		final Collection<Transition> transitions = State.this.getAutomaton().getTransitions();
		
		final StateSet stateSet = StateSet.create();
		for (final Transition current : transitions) {
			if (!previouslyVisitiedTransitions.contains(current)) {
				if (current.getLeft().equals(this)) {
					previouslyVisitiedTransitions.add(current);
					if (current.getC().equals(c)) {
						stateSet.addState(current.getRight());
						stateSet.addStates(
								current.getRight().get(EmptyWord.getInstance(), previouslyVisitiedTransitions));
					} else if (current.getC().equals(EmptyWord.getInstance())) {
						stateSet.addStates(current.getRight().get(c, previouslyVisitiedTransitions));
					}
				}
			}
		}
		return stateSet;
	}
	
	public Automaton getAutomaton() {
		return this.automaton;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
