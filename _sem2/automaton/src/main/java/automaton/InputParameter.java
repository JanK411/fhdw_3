package automaton;

public abstract class InputParameter {
	
	public abstract StateSet accept(final InputParameterVisitor v);
	
}
