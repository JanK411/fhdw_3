package automaton;

import java.util.Collection;

public class Configuration {
	
	private StateSet stateSet;
	
	private Configuration(final StateSet stateSet) {
		this.setStateSet(stateSet);
	}
	
	public static Configuration create(final State... states) {
		final StateSet stateSet = StateSet.create();
		for (final State current : states) {
			stateSet.addState(current);
		}
		
		return new Configuration(stateSet);
	}
	
	public void step(final InputParameter c) {
		final StateSet newStateSet = StateSet.create();
		for (final State current : this.getStateSet()) {
			newStateSet.addStates(current.get(c));
		}
		this.setStateSet(newStateSet);
	}
	
	public void step(final Collection<InputParameter> input) {
		for (final InputParameter current : input) {
			this.step(current);
		}
	}
	
	public boolean isEndConfiguration(final Automaton aut) {
		return this.getStateSet().isEndHavingSet(aut);
	}
	
	public StateSet getStateSet() {
		return this.stateSet;
	}
	
	public void setStateSet(final StateSet stateSet) {
		this.stateSet = stateSet;
	}
	
	@Override
	public String toString() {
		return this.stateSet.toString();
	}
	
}
