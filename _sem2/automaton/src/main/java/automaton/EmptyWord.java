package automaton;

public class EmptyWord extends InputParameter {
	private EmptyWord() {
		
	}
	
	private static EmptyWord instance;
	
	public static EmptyWord getInstance() {
		if (instance == null) {
			instance = new EmptyWord();
		}
		return instance;
	}
	
	@Override
	public String toString() {
		return "\u03B5";
	}
	
	@Override
	public StateSet accept(final InputParameterVisitor v) {
		return v.handle(this);
	}
	
}
