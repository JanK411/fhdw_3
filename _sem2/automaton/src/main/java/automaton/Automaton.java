package automaton;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Automaton {
	private final StateSet states;
	private State startState;
	private State endState;
	private final Collection<Transition> transitions;
	
	private Automaton(final StateSet states, final State startState, final State endState,
			final Collection<Transition> transitions) {
		this.startState = startState;
		this.endState = endState;
		this.states = states;
		this.transitions = transitions;
	}
	
	public State getStartState() {
		return this.startState;
	}
	
	public void setStartState(final State startState) {
		if (startState.equals(this.endState)) {
			throw new IllegalArgumentException("Start state and End state have to be different!");
		}
		this.startState = startState;
	}
	
	public State getEndState() {
		return this.endState;
	}
	
	public void setEndState(final State endState) {
		if (endState.equals(this.startState)) {
			throw new IllegalArgumentException("Start state and End state have to be different!");
		}
		this.endState = endState;
	}
	
	public void addTransition(final Transition transition) {
		this.getTransitions().add(transition);
	}
	
	public Collection<Transition> getTransitions() {
		return this.transitions;
	}
	
	public void removeUselessStates() {
		// allwort
	}

	private void removeTransitionHaving(final State state) {
		for (final Iterator<Transition> iterator = this.transitions.iterator(); iterator.hasNext();) {
			final Transition current = iterator.next();
			if (current.getLeft().equals(state)|| current.getRight().equals(state)) {
				iterator.remove();
			}
		}
	}

	public boolean recognizes(String input) {
		if (input.equals("")) {
			return this.recognizes(EmptyWord.getInstance());
		}
		
		final Configuration config = Configuration.create(this.startState);
		
		while (input.isEmpty() == false) {
			config.step(InputCharacter.create(input.charAt(0)));
			input = input.substring(1);
		}
		
		return config.isEndConfiguration(this);
	}
	
	public boolean recognizes(final List<InputParameter> parameters) {
		final Configuration config = Configuration.create(this.startState);
		
		for (final InputParameter current : parameters) {
			config.step(current);
		}
		
		return config.isEndConfiguration(this);
	}
	
	public boolean recognizes(final InputParameter param) {
		final Configuration config = Configuration.create(this.startState);
		
		config.step(param);
		
		return config.isEndConfiguration(this);
		
	}
	
	public static Automaton createAutomatonRecognizing(final char c) {
		final Automaton aut = Automaton.createZeroAutomaton();
		aut.getStartState().add(InputCharacter.create(c), aut.getEndState());
		return aut;
	}
	
	public static Automaton createAutomatonRecognizing(final InputParameter param) {
		final Automaton aut = Automaton.createZeroAutomaton();
		aut.getStartState().add(param, aut.getEndState());
		return aut;
	}
	
	public static Automaton createZeroAutomaton() {
		final Automaton aut = new Automaton(StateSet.create(), null, null, new LinkedList<>());
		
		final State startState = State.create(aut);
		aut.setStartState(startState);
		
		final State endState = State.create(aut);
		aut.setEndState(endState);
		
		return aut;
	}
	
	public static Automaton createChoiceAutomaton(final List<Automaton> automatons) {
		final Automaton aut = Automaton.createZeroAutomaton();
		
		for (final Automaton current : automatons) {
			aut.addParallel(current);
		}
		return aut;
	}
	
	/**
	 * fügt den Automaten {@code aut} an {@code this} parallel hinzu
	 * 
	 * @param aut
	 *            hinzuzufügender Automat
	 */
	public void addParallel(final Automaton aut) {
		this.startState.add(EmptyWord.getInstance(), aut.startState);
		aut.endState.add(EmptyWord.getInstance(), this.endState);
	}
	
	public static Automaton createSequenceAutomaton(final List<Automaton> automatons) {
		final Automaton aut = Automaton.createIdAutomaton();
		
		for (final Automaton current : automatons) {
			aut.concat(current);
		}
		return aut;
	}
	
	private void concat(final Automaton aut) {
		this.getEndState().add(EmptyWord.getInstance(), aut.getStartState());
		this.setEndState(aut.getEndState());
	}
	
	private static Automaton createIdAutomaton() {
		final Automaton aut = new Automaton(StateSet.create(), null, null, new LinkedList<>());
		
		final State startState = State.create(aut);
		aut.setStartState(startState);
		
		final State endState = State.create(aut);
		aut.setEndState(endState);
		
		startState.add(EmptyWord.getInstance(), endState);
		
		return aut;
	}
	
	/**
	 * Erstellt einen Automaten, der die jeweils einzelnen Character aus
	 * {@code seperateChars } erkennt.
	 * 
	 * @param seperateChars
	 *            Bsp: "abcde" für Erkennung von a || b || c || d || e.
	 * @return Automat
	 */
	public static Automaton createChoiceAutomaton(final String seperateChars) {
		String s = seperateChars;
		final Automaton automaton = createZeroAutomaton();
		while (s.isEmpty() == false) {
			automaton.startState.add(s.charAt(0), automaton.endState);
			s = s.substring(1);
		}
		
		return automaton;
	}
	
	public static Automaton createSequenceAutomaton(final String seperateChars) {
		String s = seperateChars;
		final Automaton aut = new Automaton(StateSet.create(), null, null, new LinkedList<>());
		
		State preCache = State.create(aut);
		aut.setStartState(preCache);
		State nextCache = State.create(aut);
		while (s.isEmpty() == false) {
			preCache.add(s.charAt(0), nextCache);
			preCache = nextCache;
			nextCache = State.create(aut);
			s = s.substring(1);
		}
		
		aut.setEndState(preCache);
		return aut;
	}
	
	public void setIterated() {
		this.endState.add(EmptyWord.getInstance(), this.startState);
	}
	
	public void setOptional() {
		this.startState.add(EmptyWord.getInstance(), this.endState);
	}
	
	@Override
	public String toString() {
		return this.transitions.toString();
	}
	
	public void addStateToStateSet(final State state) {
		this.states.addState(state);
	}
	
	
}
