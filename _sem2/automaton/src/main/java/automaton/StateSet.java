package automaton;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class StateSet implements Iterable<State> {
	Set<State> states;
	
	private StateSet(final Set<State> states) {
		this.states = states;
	}
	
	public static StateSet create() {
		final StateSet stateSet = new StateSet(new LinkedHashSet<>());
		return stateSet;
	}
	
	public static StateSet create(final StateSet stateSet) {
		return new StateSet(new LinkedHashSet<>(stateSet.states));
	}
	
	public StateSet addState(final State state) {
		this.states.add(state);
		return this;
	}
	
	@Override
	public Iterator<State> iterator() {
		return this.states.iterator();
	}
	
	public void addStates(final StateSet stateSet) {
		for (final State current : stateSet) {
			this.states.add(current);
		}
	}
	
	public boolean isEndHavingSet(final Automaton aut) {
		final State endState = aut.getEndState();
		for (final State current : this.states) {
			if (endState.equals(current)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.states.toString();
	}

}
