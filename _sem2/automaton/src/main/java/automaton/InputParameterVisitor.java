package automaton;

public interface InputParameterVisitor {
	
	StateSet handle(EmptyWord emptyWord);
	
	StateSet handle(InputCharacter inputCharacter);
}
