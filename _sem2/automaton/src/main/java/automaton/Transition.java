package automaton;

public class Transition {
	
	private final State left;
	private final InputParameter c;
	private final State right;
	
	private Transition(final State left, final InputParameter c, final State right) {
		this.left = left;
		this.c = c;
		this.right = right;
	}
	
	public static Transition create(final State left, final InputParameter c, final State right) {
		return new Transition(left, c, right);
	}
	
	public State getLeft() {
		return this.left;
	}
	
	public InputParameter getC() {
		return this.c;
	}
	
	public State getRight() {
		return this.right;
	}
	
	@Override
	public String toString() {
		return "-" + this.left + "--" + this.c + "--" + this.right + "->";
	}
}
