package automaton;

public class InputCharacter extends InputParameter {
	private final char c;
	
	private InputCharacter(final char c) {
		this.c = c;
	}
	
	public static InputCharacter create(final char c) {
		return new InputCharacter(c);
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof InputCharacter) {
			return this.c == ((InputCharacter) obj).c;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.valueOf(this.c);
	}
	
	@Override
	public StateSet accept(final InputParameterVisitor v) {
		return v.handle(this);
	}
}
