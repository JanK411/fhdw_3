package automaton;

import junit.framework.TestCase;

public class Test extends TestCase {
	
	public void test1() {
		final char c$Type = 'c';
		final char d$Type = 'd';
		final char e$Type = 'e';
		String testInput$1 = "";
		testInput$1 += c$Type;
		final Automaton aut = Automaton
				.createAutomatonRecognizing(c$Type); /* sa -- c --> se */
		assertEquals(true, aut.recognizes(testInput$1));
		testInput$1 += 'c'; // cc
		assertEquals(false, aut.recognizes(testInput$1));
		aut.getEndState().add(c$Type, aut.getEndState()); /* se -- c --> se */
		assertEquals(true, aut.recognizes(testInput$1));
		testInput$1 += 'd'; // ccd
		assertEquals(false, aut.recognizes(testInput$1));
		aut.getEndState().add(d$Type, aut.getStartState()); /* se -- d --> sa */
		assertEquals(false, aut.recognizes(testInput$1));
		testInput$1 += 'c'; // ccdc
		assertEquals(true, aut.recognizes(testInput$1));
		final State s1 = State.create(aut);
		aut.getStartState().add(e$Type, s1); /* sa -- e --> s1 */
		testInput$1 += 'e'; // ccdce
		assertEquals(false, aut.recognizes(testInput$1));
		s1.add(e$Type, aut.getEndState()); /* s1 -- e --> se */
		assertEquals(false, aut.recognizes(testInput$1));
		testInput$1 += 'e'; // ccdcee
		assertEquals(false, aut.recognizes(testInput$1));
		aut.getEndState().add(e$Type, aut.getStartState()); /* se -- e --> sa */
		assertEquals(false, aut.recognizes(testInput$1));
		testInput$1 += 'e'; // ccdceee
		assertEquals(true, aut.recognizes(testInput$1));
		testInput$1 += 'e'; // ccdceeee
		testInput$1 += 'e'; // ccdceeeee
		testInput$1 += 'e'; // ccdceeeeee
		assertEquals(true, aut.recognizes(testInput$1));
		aut.getEndState().add(e$Type, s1); /* se -- e --> s1 */
		assertEquals(true, aut.recognizes(testInput$1));
		testInput$1 += 'e'; // ccdceeeeeee
		assertEquals(true, aut.recognizes(testInput$1));
		testInput$1 += 'e'; // ccdceeeeeeee
		assertEquals(true, aut.recognizes(testInput$1));
		testInput$1 += 'd'; // ccdceeeeeeeed
		assertEquals(false, aut.recognizes(testInput$1));
		testInput$1 += 'e'; // ccdceeeeeeeede
		assertEquals(false, aut.recognizes(testInput$1));
		testInput$1 += 'e'; // ccdceeeeeeeedee
		assertEquals(true, aut.recognizes(testInput$1));
	}
	
	public void testEmptyWord() {
		State.i = 0;
		final Automaton aut = Automaton.createZeroAutomaton();
		assertFalse(aut.recognizes(EmptyWord.getInstance()));
		assertFalse(aut.recognizes(""));
		
		aut.getStartState().add(EmptyWord.getInstance(),
				aut.getEndState()); /* -(z0)--epsilon--((z1))-> */
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		assertTrue(aut.recognizes(""));
	}
	
	public void testEmptyWordManual() {
		final Automaton aut = Automaton.createZeroAutomaton();
		aut.addParallel(Automaton.createAutomatonRecognizing(EmptyWord.getInstance()));
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		assertTrue(aut.recognizes(""));
	}
	
	public void testChoice() {
		State.i = 0;
		final Automaton aut = Automaton.createChoiceAutomaton("abcde");
		assertTrue(aut.recognizes("a"));
		assertTrue(aut.recognizes("b"));
		assertTrue(aut.recognizes("c"));
		assertTrue(aut.recognizes("d"));
		assertTrue(aut.recognizes("e"));
		assertFalse(aut.recognizes("f"));
		assertFalse(aut.recognizes("ab"));
		assertFalse(aut.recognizes("abcde"));
		assertFalse(aut.recognizes(EmptyWord.getInstance()));
		assertFalse(aut.recognizes(""));
		
		aut.addParallel(Automaton.createAutomatonRecognizing('f'));
		
		assertTrue(aut.recognizes("f"));
		
		aut.setIterated();
		
		assertFalse(aut.recognizes(""));
		
		assertTrue(aut.recognizes("ab"));
		assertTrue(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertTrue(aut.recognizes("ababab"));
		assertTrue(aut.recognizes("afffffffffffffa"));
		assertTrue(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertTrue(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
		
	}
	
	public void testSequence() {
		State.i = 0;
		final Automaton aut = Automaton.createSequenceAutomaton("abcde");
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("b"));
		assertFalse(aut.recognizes("c"));
		assertFalse(aut.recognizes("d"));
		assertFalse(aut.recognizes("e"));
		assertFalse(aut.recognizes("f"));
		assertFalse(aut.recognizes("ab"));
		assertFalse(aut.recognizes("abcdeabcde"));
		assertFalse(aut.recognizes(EmptyWord.getInstance()));
		assertFalse(aut.recognizes(""));
		
		aut.setIterated();
		
		assertTrue(aut.recognizes("abcdeabcde"));
		assertTrue(aut.recognizes(
				"abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde"));
		
		assertFalse(aut.recognizes(""));
		
		assertFalse(aut.recognizes("ab"));
		assertFalse(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertFalse(aut.recognizes("ababab"));
		assertFalse(aut.recognizes("afffffffffffffa"));
		assertFalse(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
		
	}
	
	public void testOptionalChoice() {
		State.i = 0;
		final Automaton aut = Automaton.createChoiceAutomaton("abcde");
		aut.setOptional();
		
		assertTrue(aut.recognizes("a"));
		assertTrue(aut.recognizes("b"));
		assertTrue(aut.recognizes("c"));
		assertTrue(aut.recognizes("d"));
		assertTrue(aut.recognizes("e"));
		assertFalse(aut.recognizes("f"));
		assertFalse(aut.recognizes("ab"));
		assertFalse(aut.recognizes("abcde"));
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		assertTrue(aut.recognizes(""));
		
		aut.addParallel(Automaton.createAutomatonRecognizing('f'));
		
		assertTrue(aut.recognizes("f"));
		
		aut.setIterated();
		
		assertTrue(aut.recognizes("ab"));
		assertTrue(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertTrue(aut.recognizes("ababab"));
		assertTrue(aut.recognizes("afffffffffffffa"));
		assertTrue(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertTrue(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
		
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		assertTrue(aut.recognizes(""));
		
	}
	
	public void testOptionalChoiceInv() {
		State.i = 0;
		final Automaton aut = Automaton.createChoiceAutomaton("abcde");
		
		aut.setIterated();
		
		assertTrue(aut.recognizes("ab"));
		assertTrue(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertTrue(aut.recognizes("ababab"));
		assertFalse(aut.recognizes("afffffffffffffa"));
		assertFalse(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
		
		assertFalse(aut.recognizes(EmptyWord.getInstance()));
		assertFalse(aut.recognizes(""));
		
		aut.setOptional();
		
		assertTrue(aut.recognizes("ab"));
		assertTrue(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertTrue(aut.recognizes("ababab"));
		assertFalse(aut.recognizes("afffffffffffffa"));
		assertFalse(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
		assertTrue(aut.recognizes("a"));
		assertTrue(aut.recognizes("b"));
		assertTrue(aut.recognizes("c"));
		assertTrue(aut.recognizes("d"));
		assertTrue(aut.recognizes("e"));
		assertFalse(aut.recognizes("f"));
		assertTrue(aut.recognizes("ab"));
		assertTrue(aut.recognizes("abcde"));
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		assertTrue(aut.recognizes(""));
		
		aut.addParallel(Automaton.createAutomatonRecognizing('f'));
		
		assertTrue(aut.recognizes("ab"));
		assertTrue(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertTrue(aut.recognizes("ababab"));
		assertTrue(aut.recognizes("afffffffffffffa"));
		assertTrue(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertTrue(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
		assertTrue(aut.recognizes("a"));
		assertTrue(aut.recognizes("b"));
		assertTrue(aut.recognizes("c"));
		assertTrue(aut.recognizes("d"));
		assertTrue(aut.recognizes("e"));
		assertTrue(aut.recognizes("f"));
		assertTrue(aut.recognizes("ab"));
		assertTrue(aut.recognizes("abcde"));
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		assertTrue(aut.recognizes(""));
		
		assertTrue(aut.recognizes("f"));
		
	}
	
	public void testCrazyEpsilonThings() {
		State.i = 0;
		final Automaton aut = Automaton.createZeroAutomaton();
		final State z2 = State.create(aut);
		aut.getStartState().add(EmptyWord.getInstance(), z2);
		z2.add(EmptyWord.getInstance(), aut.getStartState());
		z2.add('a', aut.getEndState());
		
		assertTrue(aut.recognizes("a"));
	}
	
	
	public void testOptionalSequence() {
		State.i = 0;
		final Automaton aut = Automaton.createSequenceAutomaton("abcde");
		aut.setOptional();
		
		assertTrue(aut.recognizes(""));
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("b"));
		assertFalse(aut.recognizes("c"));
		assertFalse(aut.recognizes("d"));
		assertFalse(aut.recognizes("e"));
		assertFalse(aut.recognizes("f"));
		assertFalse(aut.recognizes("ab"));
		assertFalse(aut.recognizes("abcdeabcde"));
		
		aut.setIterated();
		
		assertTrue(aut.recognizes("abcdeabcde"));
		assertTrue(aut.recognizes(
				"abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde"));
		
		assertTrue(aut.recognizes(""));
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
		
		assertFalse(aut.recognizes("ab"));
		assertFalse(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertFalse(aut.recognizes("ababab"));
		assertFalse(aut.recognizes("afffffffffffffa"));
		assertFalse(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
	}
	
	
	

	public void testOptionalSequenceInv() {
		State.i = 0;
		final Automaton aut = Automaton.createSequenceAutomaton("abcde");
		
		aut.setIterated();
		
		assertTrue(aut.recognizes("abcdeabcde"));
		assertTrue(aut.recognizes(
				"abcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcdeabcde"));
		
		assertFalse(aut.recognizes(""));
		assertFalse(aut.recognizes(EmptyWord.getInstance()));
		
		assertFalse(aut.recognizes("ab"));
		assertFalse(aut.recognizes("aaaaaaaaaaaaaaa"));
		assertFalse(aut.recognizes("ababab"));
		assertFalse(aut.recognizes("afffffffffffffa"));
		assertFalse(aut.recognizes("fffffffffffff"));
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("abcdef"));
		assertFalse(aut.recognizes("abcdoef"));
		
		aut.setOptional();
		
		assertTrue(aut.recognizes("abcde"));
		assertFalse(aut.recognizes("b"));
		assertFalse(aut.recognizes("c"));
		assertFalse(aut.recognizes("d"));
		assertFalse(aut.recognizes("e"));
		assertFalse(aut.recognizes("f"));
		assertFalse(aut.recognizes("ab"));
		assertTrue(aut.recognizes("abcdeabcde"));
		assertTrue(aut.recognizes(""));
		assertTrue(aut.recognizes(EmptyWord.getInstance()));
	}
	
}
