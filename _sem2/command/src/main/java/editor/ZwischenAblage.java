package editor;

public class ZwischenAblage {
	private static ZwischenAblage instance;
	private String text;

	private ZwischenAblage() {
		this.text = "";
	}
	
	public static ZwischenAblage getInstance() {
		if (instance == null) {
			instance = new ZwischenAblage();
		}
		return instance;
	}
	
	public void setText(final String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
}
