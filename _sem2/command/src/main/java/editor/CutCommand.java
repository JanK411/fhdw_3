package editor;

import editor.commands.Command;

public class CutCommand extends Command {
	
	private CutCommand(final Editor editor) {
		super(editor);
	}

	@Override
	public void execute() {
		int firstPos = this.editor.getFirstPosition();
		int secondPos = this.editor.getSecondPosition();
		
		if (firstPos > secondPos){
			final int buffer = firstPos;
			firstPos = secondPos;
			secondPos = buffer;
		}
		
		final String text = this.editor.getText().substring(firstPos, secondPos);
		ZwischenAblage.getInstance().setText(text);
		this.editor.getText().replace(firstPos, secondPos, "");
	}
	
	@Override
	public void undo() {
		PasteCommand.create(this.editor).execute();
	}

	public static CutCommand create(final Editor editor) {
		return new CutCommand(editor);
	}
	
}
