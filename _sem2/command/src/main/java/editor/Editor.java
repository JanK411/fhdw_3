package editor;

import java.util.Stack;

import editor.commands.Command;
import editor.commands.CopyCommand;
import editor.commands.DeleteLeftCommand;
import editor.commands.DeleteRightCommand;
import editor.commands.GoLeftCommand;
import editor.commands.GoRightCommand;
import editor.commands.KeyTypedCommand;
import editor.commands.NewLineCommand;

public class Editor {
	
	Stack<Command> undo;
	Stack<Command> redo;
	
	public static Editor createEditor() {
		return new Editor();
	}
	
	private final StringBuffer text;
	private int firstPosition;
	private int secondPosition;
	private boolean shiftMode;
	
	private Editor() {
		this.text = new StringBuffer();
		this.firstPosition = 0;
		this.secondPosition = 0;
		this.shiftMode = false;
		this.undo = new Stack<>();
		this.redo = new Stack<>();
	}
	
	public void keyTyped(final Character c) {
		final KeyTypedCommand command = KeyTypedCommand.create(c, this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public StringBuffer getText() {
		return this.text;
	}
	
	public int getPosition() {
		return this.getFirstPosition();
	}
	
	public int getFirstPosition() {
		return this.firstPosition;
	}
	
	public void setPosition(final int position) {
		this.setFirstPosition(position);
		this.setSecondPosition(position);
	}
	
	public void setFirstPosition(final int position) {
		this.firstPosition = position;
	}
	
	public int getSecondPosition() {
		return this.secondPosition;
	}
	
	private void setSecondPosition(final int position) {
		this.secondPosition = position;
	}
	
	public void deleteLeft() {
		final DeleteLeftCommand command = DeleteLeftCommand.create(this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public void deleteRight() {
		final DeleteRightCommand command = DeleteRightCommand.create(this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public void left() {
		final GoLeftCommand command = GoLeftCommand.create(this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public void right() {
		final GoRightCommand command = GoRightCommand.create(this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public void newLine() {
		final NewLineCommand command = NewLineCommand.create(this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public String getEditorText() {
		return this.getText().toString();
	}
	
	public void shift() {
		this.setShiftMode(!this.getShiftMode());
	}
	
	private void setShiftMode(final boolean b) {
		this.shiftMode = b;
	}
	
	public boolean getShiftMode() {
		return this.shiftMode;
	}
	
	public void undo() {
		if (this.undo.isEmpty() == false) {
			final Command command = this.undo.pop();
			command.undo();
			this.redo.push(command);
		} else {
			System.out.println("alle!!!");
		}
	}
	
	public void redo() {
		if (this.redo.isEmpty() == false) {
			final Command command = this.redo.pop();
			command.execute();
			this.undo.push(command);
		} else {
			System.out.println("alle¡¡¡");
		}
	}
	
	public void copy() {
		final CopyCommand command = CopyCommand.create(this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public void cut() {
		final CutCommand command = CutCommand.create(this);
		command.execute();
		this.pushToUndoStackAndClearRedoStack(command);
	}
	
	public void paste() {
		// TODO Provide method for operation "paste"
	}
	
	private void pushToUndoStackAndClearRedoStack(final Command command) {
		this.undo.push(command);
		this.redo.clear();
	}
	
}
