package editor.commands;

import editor.Editor;

public class KeyTypedCommand extends CommandThatHasACharNamedCMemorized {
	
	public KeyTypedCommand(final Character c, final Editor editor) {
		super(editor, c);
	}
	
	public static KeyTypedCommand create(final Character c, final Editor editor) {
		return new KeyTypedCommand(c, editor);
	}
	
	@Override
	public void execute() {
		this.editor.getText().insert(this.editor.getPosition(),
				this.editor.getShiftMode() ? this.c : Character.toLowerCase(this.c));
		this.editor.setPosition(this.editor.getPosition() + 1);
	}
	
	@Override
	public void undo() {
		DeleteLeftCommand.create(this.editor).execute();
	}
	
}
