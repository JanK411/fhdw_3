package editor.commands;

import editor.Editor;
import editor.ZwischenAblage;

public class CopyCommand extends Command {
	
	private CopyCommand(final Editor editor) {
		super(editor);
	}
	
	@Override
	public void execute() {
		int firstPos = this.editor.getFirstPosition();
		int secondPos = this.editor.getSecondPosition();
		
		if (firstPos > secondPos){
			final int buffer = firstPos;
			firstPos = secondPos;
			secondPos = buffer;
		}
		
		final String text = this.editor.getText().substring(firstPos, secondPos);
		ZwischenAblage.getInstance().setText(text);
	}
	
	@Override
	public void undo() {
		// done.
	}
	
	public static CopyCommand create(final Editor editor) {
		return new CopyCommand(editor);
	}
}
