package editor.commands;

import editor.Editor;

public class GoRightCommand extends Command {
	
	private GoRightCommand(final Editor editor) {
		super(editor);
	}
	
	@Override
	public void execute() {
		if (this.editor.getPosition() < this.editor.getText().length()) {
			if (this.editor.getShiftMode()) {
				this.editor.setFirstPosition(this.editor.getFirstPosition() + 1);
			} else {
				this.editor.setPosition(this.editor.getPosition() + 1);
			}
		}
	}
	
	@Override
	public void undo() {
		GoLeftCommand.create(this.editor).execute();
	}
	
	public static GoRightCommand create(final Editor editor) {
		return new GoRightCommand(editor);
	}
	
}
