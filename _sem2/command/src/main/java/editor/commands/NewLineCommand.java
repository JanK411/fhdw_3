package editor.commands;

import editor.Editor;

public class NewLineCommand extends Command {
	
	private NewLineCommand(final Editor editor) {
		super(editor);
	}
	
	@Override
	public void execute() {
		this.editor.getText().insert(this.editor.getPosition(), "\n");
		this.editor.setPosition(this.editor.getPosition() + 1);
	}
	
	@Override
	public void undo() {
		DeleteLeftCommand.create(this.editor).execute();
	}
	
	public static NewLineCommand create(final Editor editor) {
		return new NewLineCommand(editor);
	}
	
}
