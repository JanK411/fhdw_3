package editor.commands;

import editor.Editor;

public class GoLeftCommand extends Command {
	
	private GoLeftCommand(final Editor editor) {
		super(editor);
	}
	
	@Override
	public void execute() {
		if (this.editor.getPosition() > 0) {
			if (this.editor.getShiftMode()) {
				this.editor.setFirstPosition(this.editor.getFirstPosition() - 1);
			} else {
				this.editor.setPosition(this.editor.getPosition() - 1);
			}
		}
	}
	
	@Override
	public void undo() {
		GoRightCommand.create(this.editor).execute();
	}
	
	public static GoLeftCommand create(final Editor editor) {
		return new GoLeftCommand(editor);
	}
	
}
