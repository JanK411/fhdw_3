package editor.commands;

import editor.Editor;

public class DeleteRightCommand extends CommandThatHasACharNamedCMemorized {
	
	
	private DeleteRightCommand(final Editor editor, final char c) {
		super(editor, c);
	}
	
	@Override
	public void execute() {
		final int pos = this.editor.getPosition();
		this.editor.getText().deleteCharAt(pos);
	}
	
	@Override
	public void undo() {
		KeyTypedCommand.create(this.c, this.editor).execute();
		GoLeftCommand.create(this.editor).execute();
	}
	
	public static DeleteRightCommand create(final Editor editor) {
		return new DeleteRightCommand(editor, editor.getText().charAt(editor.getPosition()));
	}
	
}
