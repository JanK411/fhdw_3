package editor.commands;

import editor.Editor;

public abstract class Command {
	protected final Editor editor;
	
	public Command(final Editor editor) {
		this.editor = editor;
	}
	
	public abstract void execute();
	
	public abstract void undo();
	
}
