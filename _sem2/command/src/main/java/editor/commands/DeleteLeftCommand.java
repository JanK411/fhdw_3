package editor.commands;

import editor.Editor;

public class DeleteLeftCommand extends CommandThatHasACharNamedCMemorized {
	
	private DeleteLeftCommand(final Editor editor, final char c) {
		super(editor, c);
	}
	
	@Override
	public void execute() {
		final int pos = this.editor.getPosition();
		this.editor.getText().deleteCharAt(pos - 1); 
		this.editor.setPosition(pos - 1);
	}
	
	@Override
	public void undo() {
		KeyTypedCommand.create(this.c, this.editor).execute();
	}
	
	public static DeleteLeftCommand create(final Editor editor) {
		return new DeleteLeftCommand(editor, editor.getText().charAt(editor.getPosition() - 1));
	}
	
}
