package editor.commands;

import editor.Editor;

public abstract class CommandThatHasACharNamedCMemorized extends Command {
	
	protected final char c;
	
	protected CommandThatHasACharNamedCMemorized(final Editor editor, final char c) {
		super(editor);
		this.c = c;
	}
}
