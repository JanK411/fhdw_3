package view;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import editor.Editor;

public class View extends JFrame {

	private static final long serialVersionUID = 1L;

	private Editor editor;

	private JPanel jContentPane = null;

	private JSplitPane mainSplitPane = null;

	private JScrollPane textScrollPane = null;

	private JTextArea textArea = null;

	private JPanel keyPanel = null;


	/**
	 * This is the default constructor
	 */
	public View() {
		super();
		this.initialize();
	}

	public View(final Editor facade) {
		super();
		this.editor = facade;
		this.initialize();
		this.refresh();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(810, 412);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(this.getJContentPane());
		this.setTitle("Editor");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (this.jContentPane == null) {
			this.jContentPane = new JPanel();
			this.jContentPane.setLayout(new BorderLayout());
			this.jContentPane.add(this.getMainSplitPane(), BorderLayout.CENTER);
		}
		return this.jContentPane;
	}

	/**
	 * This method initializes mainSplitPane	
	 * 	
	 * @return javax.swing.JSplitPane	
	 */
	private JSplitPane getMainSplitPane() {
		if (this.mainSplitPane == null) {
			this.mainSplitPane = new JSplitPane();
			this.mainSplitPane.setDividerSize(5);
			this.mainSplitPane.setDividerLocation(230);
			this.mainSplitPane.setBottomComponent(this.getKeyPanel());
			this.mainSplitPane.setTopComponent(this.getTextPanel());
			this.mainSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		}
		return this.mainSplitPane;
	}

	/**
	 * This method initializes textScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getTextScrollPane() {
		if (this.textScrollPane == null) {
			this.textScrollPane = new JScrollPane();
			this.textScrollPane.setViewportView(this.getTextArea());
		}
		return this.textScrollPane;
	}

	/**
	 * This method initializes textArea	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getTextArea() {
		if (this.textArea == null) {
			this.textArea = new JTextArea();
			this.textArea.setEditable(false);
		}
		return this.textArea;
	}

	/**
	 * This method initializes keyPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getKeyPanel() {
		if (this.keyPanel == null) {
			this.keyPanel = new JPanel();
			this.keyPanel.setLayout(new GridBagLayout());
			this.addButton(new LetterButton('Q'), 0, 0);
			this.addButton(new LetterButton('W'), 1, 0);
			this.addButton(new LetterButton('E'), 2, 0);
			this.addButton(new LetterButton('R'), 3, 0);
			this.addButton(new LetterButton('T'), 4, 0);
			this.addButton(new LetterButton('Z'), 5, 0);
			this.addButton(new LetterButton('U'), 6, 0);
			this.addButton(new LetterButton('I'), 7, 0);
			this.addButton(new LetterButton('O'), 8, 0);
			this.addButton(new LetterButton('P'), 9, 0);
			this.addButton(new LetterButton('Ü'), 10, 0);
			this.addButton(new LetterButton('+'), 11, 0);
			this.addDistance(12,0);
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 13;
			gridBagConstraints.gridy = 0;
			this.keyPanel.add(this.getDeleteLeftButton(),gridBagConstraints);
			gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 14;
			gridBagConstraints.gridy = 0;
			this.keyPanel.add(this.getDeleteRightButton(),gridBagConstraints);
			
			this.addButton(new LetterButton('A'), 0, 1);
			this.addButton(new LetterButton('S'), 1, 1);
			this.addButton(new LetterButton('D'), 2, 1);
			this.addButton(new LetterButton('F'), 3, 1);
			this.addButton(new LetterButton('G'), 4, 1);
			this.addButton(new LetterButton('H'), 5, 1);
			this.addButton(new LetterButton('J'), 6, 1);
			this.addButton(new LetterButton('K'), 7, 1);
			this.addButton(new LetterButton('L'), 8, 1);
			this.addButton(new LetterButton('Ö'), 9, 1);
			this.addButton(new LetterButton('Ä'), 10, 1);
			this.addButton(new LetterButton('#'), 11, 1);
			this.addDistance(12,1);
			gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 13;
			gridBagConstraints.gridy = 1;
			this.keyPanel.add(this.getLeftButton(),gridBagConstraints);
			gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 14;
			gridBagConstraints.gridy = 1;
			this.keyPanel.add(this.getRightButton(),gridBagConstraints);

			gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.gridy = 2;
			this.keyPanel.add(this.getShiftButton1(),gridBagConstraints);
			this.addButton(new LetterButton('Y'), 1, 2);
			this.addButton(new LetterButton('X'), 2, 2);
			this.addButton(new LetterButton('C'), 3, 2);
			this.addButton(new LetterButton('V'), 4, 2);
			this.addButton(new LetterButton('B'), 5, 2);
			this.addButton(new LetterButton('N'), 6, 2);
			this.addButton(new LetterButton('M'), 7, 2);
			this.addButton(new LetterButton(','), 8, 2);
			this.addButton(new LetterButton('.'), 9, 2);
			this.addButton(new LetterButton('_'), 10, 2);
			gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 11;
			gridBagConstraints.gridy = 2;
			this.keyPanel.add(this.getShiftButton2(),gridBagConstraints);
			gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 13;
			gridBagConstraints.gridy = 2;
			this.keyPanel.add(this.getNewLineButton(),gridBagConstraints);
			
			this.addButton(new LetterButton(' '), 4, 3);
			this.addButton(new LetterButton(' '), 5, 3);
			this.addButton(new LetterButton(' '), 6, 3);
			this.addButton(new LetterButton(' '), 7, 3);
		}
		return this.keyPanel;
	}
	private JButton newLineButton = null;
	private JButton getNewLineButton() {
		if (this.newLineButton == null){
			this.newLineButton = new JButton();
			this.newLineButton.setText("<<<----");
			this.newLineButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.getEditor().newLine();
					View.this.refresh();
				}
			});
		}
		return this.newLineButton;
	}
	private JButton deleteRightButton = null;
	private JButton getDeleteRightButton() {
		if (this.deleteRightButton == null){
			this.deleteRightButton = new JButton();
			this.deleteRightButton.setText("Entf >>");
			this.deleteRightButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.getEditor().deleteRight();
					View.this.refresh();
				}
			});
		}
		return this.deleteRightButton;
	}
	private JButton deleteLeftButton = null;
	private JButton getDeleteLeftButton() {
		if (this.deleteLeftButton == null){
			this.deleteLeftButton = new JButton();
			this.deleteLeftButton.setText("<< Entf");
			this.deleteLeftButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.getEditor().deleteLeft();
					View.this.refresh();
				}
			});
		}
		return this.deleteLeftButton;
	}
	private JToggleButton shiftButton1 = null;
	private JToggleButton getShiftButton1() {
		if (this.shiftButton1 == null){
			this.shiftButton1 = new JToggleButton();
			this.shiftButton1.setText("^");
			this.shiftButton1.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.shift_action(View.this.shiftButton1);
				}
			});
		}
		return this.shiftButton1;
	}
	protected void shift_action(final JToggleButton shiftButton) {
		this.getEditor().shift();
		this.getShiftButton1().setSelected(shiftButton.isSelected());
		this.getShiftButton2().setSelected(shiftButton.isSelected());
		this.refresh();
	}
	private JToggleButton shiftButton2 = null;
	private JToggleButton getShiftButton2() {
		if (this.shiftButton2 == null){
			this.shiftButton2 = new JToggleButton();
			this.shiftButton2.setText("^");
			this.shiftButton2.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.shift_action(View.this.shiftButton2);
				}
			});
		}
		return this.shiftButton2;
	}

	private JButton rightButton = null;
	private JButton getRightButton() {
		if (this.rightButton == null){
			this.rightButton = new JButton();
			this.rightButton.setText(">>>>>");
			this.rightButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.getEditor().right();
					View.this.refresh();
				}
			});
		}
		return this.rightButton;
	}
	private JButton leftButton = null;

	private JPanel textPanel = null;

	private JToolBar textToolBar = null;

	private JButton undoButton = null;

	private JButton redoButton = null;

	private JLabel sepratorLabel = null;

	private JButton copyButton = null;

	private JButton pasteButton = null;

	private JButton cutButton = null;
	private JButton getLeftButton() {
		if (this.leftButton == null){
			this.leftButton = new JButton();
			this.leftButton.setText("<<<<<");
			this.leftButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.getEditor().left();
					View.this.refresh();
				}
			});
		}
		return this.leftButton;
	}

	private void addDistance(final int x, final int y) {
		final GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = x;
		gridBagConstraints.gridy = y;
		this.getKeyPanel().add(new JLabel("    "),gridBagConstraints);
	}

	private void addButton (final KeyButton button, final int x, final int y){
		final GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = x;
		gridBagConstraints.gridy = y;
		this.getKeyPanel().add(button,gridBagConstraints);
	}

	@SuppressWarnings("serial")
	abstract class KeyButton extends JButton {
		
	}
	@SuppressWarnings("serial")
	class LetterButton extends KeyButton {
		
		public LetterButton(final Character c) {
			super();
			this.setText(c.toString());
			this.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(final ActionEvent e) {
					View.this.keyTyped(c);
				}
			});
		}
	}
	protected void keyTyped(final char c) {
		this.getEditor().keyTyped(c);
		this.refresh();
	}

	private void refresh() {
		this.getTextArea().setText(this.getEditor().getEditorText());
		final int firstPosition = this.getEditor().getPosition();
		this.getTextArea().setCaretPosition(firstPosition);
		final int secondPosition = this.getEditor().getSecondPosition();
		this.getTextArea().moveCaretPosition(secondPosition);
		this.getTextArea().getCaret().setVisible(true);
		this.getTextArea().getCaret().setSelectionVisible(true);
		final boolean cutPasteEnabled = firstPosition != secondPosition;
		this.getCopyButton().setEnabled(cutPasteEnabled);
		this.getCutButton().setEnabled(cutPasteEnabled);
	}

	private Editor getEditor() {
		return this.editor;
	}

	/**
	 * This method initializes textPanel	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getTextPanel() {
		if (this.textPanel == null) {
			this.textPanel = new JPanel();
			this.textPanel.setLayout(new BorderLayout());
			this.textPanel.add(this.getTextScrollPane(), BorderLayout.CENTER);
			this.textPanel.add(this.getTextToolBar(), BorderLayout.NORTH);
		}
		return this.textPanel;
	}

	/**
	 * This method initializes textToolBar	
	 * 	
	 * @return javax.swing.JToolBar	
	 */
	private JToolBar getTextToolBar() {
		if (this.textToolBar == null) {
			this.sepratorLabel = new JLabel();
			this.sepratorLabel.setText("          ");
			this.textToolBar = new JToolBar();
			this.textToolBar.add(this.getUndoButton());
			this.textToolBar.add(this.getRedoButton());
			this.textToolBar.add(this.sepratorLabel);
			this.textToolBar.add(this.getCopyButton());
			this.textToolBar.add(this.getCutButton());
			this.textToolBar.add(this.getPasteButton());
		}
		return this.textToolBar;
	}

	/**
	 * This method initializes undoButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getUndoButton() {
		if (this.undoButton == null) {
			this.undoButton = new JButton();
			this.undoButton.setText("UNDO");
			this.undoButton.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(final java.awt.event.ActionEvent e) {
					View.this.undo_action();
				}
			});
		}
		return this.undoButton;
	}

	protected void undo_action() {
		this.getEditor().undo();
		this.refresh();
	}

	/**
	 * This method initializes redoButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getRedoButton() {
		if (this.redoButton == null) {
			this.redoButton = new JButton();
			this.redoButton.setText("REDO");
			this.redoButton.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(final java.awt.event.ActionEvent e) {
					View.this.redo_action();
				}
			});
		}
		return this.redoButton;
	}

	protected void redo_action() {
		this.getEditor().redo();
		this.refresh();
	}

	/**
	 * This method initializes copyButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getCopyButton() {
		if (this.copyButton == null) {
			this.copyButton = new JButton();
			this.copyButton.setText("Kopieren");
			this.copyButton.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(final java.awt.event.ActionEvent e) {
					View.this.copy_action();
				}
			});
		}
		return this.copyButton;
	}

	protected void copy_action() {
		this.getEditor().copy();
	}

	/**
	 * This method initializes pasteButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getPasteButton() {
		if (this.pasteButton == null) {
			this.pasteButton = new JButton();
			this.pasteButton.setText("Einf�gen");
			this.pasteButton.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(final java.awt.event.ActionEvent e) {
					View.this.paste_action();
				}
			});
		}
		return this.pasteButton;
	}

	protected void paste_action() {
		this.getEditor().paste();
		this.refresh();
	}

	/**
	 * This method initializes cutButton	
	 * 	
	 * @return javax.swing.JButton	
	 */
	private JButton getCutButton() {
		if (this.cutButton == null) {
			this.cutButton = new JButton();
			this.cutButton.setText("Ausschneiden");
			this.cutButton.addActionListener(new java.awt.event.ActionListener() {
				@Override
				public void actionPerformed(final java.awt.event.ActionEvent e) {
					View.this.cut_action();
				}
			});
		}
		return this.cutButton;
	}

	protected void cut_action() {
		this.getEditor().cut();
		this.refresh();
	}
}  //  @jve:decl-index=0:visual-constraint="10,10"

