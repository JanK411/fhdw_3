package arithmeticExpression.model;

import arithmeticExpression.model.observer.Observee;

public abstract class Expression extends Observee{
	public abstract int getValue();
}
