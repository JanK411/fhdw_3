package arithmeticExpression.model.operations;

import arithmeticExpression.model.Operation;

public class NotCachedState implements OperationState {
	private NotCachedState() {
		
	}
	
	private static NotCachedState instance;
	
	public static NotCachedState getInstance() {
		if (instance == null) {
			instance = new NotCachedState();
		}
		return instance;
	}
	
	@Override
	public int getValue(final Operation operation) {
		return operation.reallyCalculateValue();
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof NotCachedState) {
			return true;
		}
		return false;
	}
	
}
