package arithmeticExpression.model.operations;

import arithmeticExpression.model.Operation;

public interface OperationState {

	int getValue(Operation operation);
	
}
