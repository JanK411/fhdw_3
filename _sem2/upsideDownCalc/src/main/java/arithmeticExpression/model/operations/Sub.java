package arithmeticExpression.model.operations;

import arithmeticExpression.model.Expression;
import arithmeticExpression.model.Operation;

public class Sub extends Operation {
	
	private Sub(final Expression firstArgument, final Expression secondArgument) {
		super(firstArgument, secondArgument);
	}
	
	public static Expression create(final Expression firstArgument, final Expression secondArgument) {
		return new Sub(firstArgument, secondArgument);
	}

	@Override
	protected String operatorSymbol() {
		return "-";
	}

	@Override
	protected int calc(final int value1, final int value2) {
		return value1 - value2;
	}
	
}
