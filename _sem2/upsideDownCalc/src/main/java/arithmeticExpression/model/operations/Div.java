package arithmeticExpression.model.operations;

import arithmeticExpression.model.Expression;
import arithmeticExpression.model.Operation;

public class Div extends Operation {
	
	public Div(final Expression firstArgument, final Expression secondArgument) {
		super(firstArgument, secondArgument);
	}
	
	public static Expression create(final Expression firstArgument, final Expression secondArgument){
		return new Div(firstArgument, secondArgument);
	}

	@Override
	protected String operatorSymbol() {
		return "/";
	}

	@Override
	protected int calc(final int value1, final int value2) {
		if (value2 == 0) {
			throw new ArithmeticException("Du teilst durch 0!¡!");
		}
		return value1 / value2;
	}
	
}
