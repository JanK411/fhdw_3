package arithmeticExpression.model.operations;

import arithmeticExpression.model.Operation;

public class CachedState implements OperationState {
	private final int value;
	
	private CachedState(final int value) {
		this.value = value;
	}
	
	public static CachedState create(final int value) {
		return new CachedState(value);
	}
	
	@Override
	public int getValue(final Operation operation) {
		return this.value;
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof CachedState) {
			final CachedState toEqual = (CachedState) obj;
			return toEqual.value == this.value;
		}
		return false;
	}
}
