package arithmeticExpression.model.observer;

public interface Observer {
	void update();
}
