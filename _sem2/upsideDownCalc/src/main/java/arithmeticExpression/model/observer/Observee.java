package arithmeticExpression.model.observer;

import java.util.LinkedHashSet;
import java.util.Set;

public abstract class Observee {
	private final Set<Observer> observers;
	
	protected Observee() {
		this.observers = new LinkedHashSet<>();
	}
	
	public void register(final Observer obs) {
		this.observers.add(obs);
	}
	
	public void deregister(final Observer obs) {
		this.observers.remove(obs);
	}
	
	protected void notifyObservers() {
		for (final Observer current : this.observers) {
			current.update();
		}
	}
	
}
