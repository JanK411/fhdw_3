package arithmeticExpression.model;

import arithmeticExpression.model.observer.Observer;
import arithmeticExpression.model.operations.CachedState;
import arithmeticExpression.model.operations.NotCachedState;
import arithmeticExpression.model.operations.OperationState;

public abstract class Operation extends Expression implements Observer {
	
	private final Expression firstArgument;
	private final Expression secondArgument;
	private OperationState state;
	
	public Operation(final Expression firstArgument, final Expression secondArgument) {
		this.firstArgument = firstArgument;
		this.secondArgument = secondArgument;
		this.state = NotCachedState.getInstance();
		
		firstArgument.register(this);
		secondArgument.register(this);
	}
	
	@Override
	public String toString() {
		return "(" + this.firstArgument + this.operatorSymbol() + this.secondArgument + ") = " + this.getValue();
	}
	
	@Override
	public int getValue() {
		return this.state.getValue(this);
	}
	
	protected abstract int calc(final int value1, final int value2);
	
	protected abstract String operatorSymbol();
	
	@Override
	public void update() {
		this.setState(NotCachedState.getInstance());
	}
	
	public int reallyCalculateValue() {
		final int ret = this.calc(this.firstArgument.getValue(), this.secondArgument.getValue());
		this.setState(CachedState.create(ret));
		return 0;
	}
	
	private void setState(final OperationState state) {
		if (this.state.equals(state) == false) {
			this.state = state;
			System.out.println("updated°°°");
			this.notifyObservers();
		}
	}
}
