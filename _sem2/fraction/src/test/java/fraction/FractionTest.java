package fraction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class FractionTest {
	
	private Fraction f6_3;
	private Fraction f1_2;
	private Fraction f1_4;
	private Fraction f2_5;
	private Fraction f2_1;
	private Fraction fn1_2;
	
	/**
	 * Functions to be run before all tests
	 */
	@Before
	public void setUp() {
		try {
			this.f6_3 = Fraction.valueOf("6/3");
			this.f1_2 = Fraction.valueOf("1/2");
			this.f1_4 = Fraction.valueOf("1/4");
			this.f2_5 = Fraction.valueOf("2/5");
			this.f2_1 = Fraction.valueOf("2/1");
			this.fn1_2 = Fraction.create(BigInteger.valueOf(-1), BigInteger.valueOf(2));
		} catch (final FractionConstructionException e) {
			throw new Error(e);
		}
	}
	
	/**
	 * Function for manual Tests
	 * 
	 * @throws
	 */
	@Ignore
	public void manual() {
		System.out.println(Fraction.harmony(BigInteger.valueOf(4)));
	}
	
	/**
	 * tests the equals function of Fraction
	 */
	@Test
	public void testEquals() {
		assertEquals(this.f6_3, this.f2_1);
		assertEquals(this.f1_2, Fraction.valueOf("2/4"));
		// TODO Testfall mit 0 && mit "richtigen Brüchen"
	}
	
	/**
	 * tests the Exception throwing of Fraction with 0 in denom
	 */
	@Test
	public void testException() {
		try {
			Fraction.valueOf("5/0");
			fail();
		} catch (final FractionConstructionException e) {
		}
		
		try {
			Fraction.valueOf("0/0");
			fail();
		} catch (final FractionConstructionException e) {
		}
	}
	
	/**
	 * tests the toString function of Fraction
	 */
	@Test
	public void testToString() {
		assertEquals("2/5", this.f2_5.toString());
	}
	
	/**
	 * tests the add function of Fraction
	 */
	@Test
	public void testAdd() {
		assertEquals(this.f1_2.add(this.f1_4), this.f1_4.add(this.f1_4).add(this.f1_4));
		assertEquals(Fraction.valueOf(1.5), this.f2_1.add(this.fn1_2));
		assertEquals(Fraction.valueOf(2.25), this.f2_1.add(this.f1_4));
		assertEquals(Fraction.valueOf(-0.1), this.f2_5.add(this.fn1_2));
	}
	
	/**
	 * tests the multiply function of Fraction
	 */
	@Test
	public void testMultiply() {
		assertEquals(this.f1_4, this.f1_2.multiply(this.f1_2));
	}
	
	/**
	 * tests the substract function of Fraction
	 */
	@Test
	public void testSubstract() {
		assertEquals(this.f1_4, this.f1_2.substract(this.f1_4));
	}
	
	@Test
	public void testPower() {
		assertEquals(Fraction.valueOf(9), Fraction.valueOf("1/3").pow(BigInteger.valueOf(-2)));
		assertEquals(Fraction.valueOf("9/4"), Fraction.valueOf("3/2").pow(BigInteger.valueOf(2)));
		
		assertEquals(Fraction.valueOf(0), Fraction.valueOf(0).pow(BigInteger.valueOf(26)));
		
		assertEquals(Fraction.ONE, this.f2_5.pow(BigInteger.ZERO));
		assertEquals(Fraction.ONE, this.fn1_2.pow(BigInteger.ZERO));
		
		assertEquals(this.fn1_2, this.fn1_2.pow(BigInteger.ONE));
		assertEquals(this.f2_5, this.f2_5.pow(BigInteger.ONE));
		
		// System.out.println("------> " +
		// f1_2.pow(BigInteger.valueOf(1500000).multiply(BigInteger.valueOf(4))));
		
	}
	
	@Test
	public void testHarmony() {
		assertEquals(BigInteger.ONE, Fraction.harmony(BigInteger.ONE));
		assertEquals(BigInteger.valueOf(4), Fraction.harmony(BigInteger.valueOf(2)));
		assertEquals(BigInteger.valueOf(11), Fraction.harmony(BigInteger.valueOf(3)));
		assertEquals(BigInteger.valueOf(31), Fraction.harmony(BigInteger.valueOf(4)));
	}
	
	public void testPowerException() {
		try {
			assertEquals(Fraction.valueOf(0), Fraction.valueOf(0).pow(BigInteger.valueOf(-26)));
			fail();
		} catch (final FractionConstructionException e) {
		}
	}
	
	/**
	 * tests the inversion function of Fraction
	 * 
	 */
	@Test
	public void testInversion() {
		assertEquals(this.f1_2.inversion(), this.f2_1);
	}
	
	/**
	 * tests the division function of Fraction
	 * 
	 */
	@Test
	public void testDivide() {
		assertEquals(this.f1_2.divide(this.f1_4), this.f2_1);
	}
	
	/**
	 * Tests the exception of valueOf
	 * 
	 */
	@Test
	public void testExceptionValueOf() {
		try {
			Fraction.valueOf("k");
			fail();
		} catch (final IllegalArgumentException e) {
		}
		
		try {
			Fraction.valueOf("Hallo0,5Welt");
			fail();
		} catch (final IllegalArgumentException e) {
		}
	}
	
	/**
	 * tests the valueOf function of Fraction with negative values
	 * 
	 * @
	 */
	@Test
	public void testValueOfNeg() {
		assertEquals(this.fn1_2, Fraction.valueOf(-0.5));
		assertEquals(this.fn1_2, Fraction.valueOf("-5E-1"));
		assertEquals(this.f1_2, Fraction.valueOf("-1/-2"));
		assertEquals(this.fn1_2, Fraction.valueOf("1/-2"));
		
		assertEquals("-1/2", Fraction.valueOf("4/-8").toString());
		assertEquals("1/2", Fraction.valueOf("-4/-8").toString());
		assertEquals("-1/2", Fraction.valueOf("-4/8").toString());
		assertEquals("1/2", Fraction.valueOf("4/8").toString());
		
	}
	
	/**
	 * tests the valueOf function of Fraction
	 */
	@Test
	public void testValueOf() {
		assertEquals(Fraction.valueOf(0), Fraction.valueOf("0/1"));
		assertEquals(Fraction.valueOf(0), Fraction.valueOf("0/42"));
		assertEquals(this.f6_3, Fraction.valueOf("120/60"));
		assertEquals(this.f1_2, Fraction.valueOf("1/2"));
		assertEquals(this.f1_2, Fraction.valueOf("/2"));
		
		assertEquals(this.f2_1, Fraction.valueOf("2"));
		
		assertEquals(this.f1_2, Fraction.valueOf("0.5"));
		assertEquals(this.f1_2, Fraction.valueOf("0,5"));
		assertEquals(this.f1_2, Fraction.valueOf(".5"));
		assertEquals(this.f1_2, Fraction.valueOf(",5"));
		
		assertEquals(this.f1_2, Fraction.valueOf("5E-1"));
		assertEquals(Fraction.valueOf("5E2"), Fraction.valueOf(500));
		assertEquals(this.f2_1, Fraction.valueOf("2E0"));
		assertEquals(Fraction.valueOf("200"), Fraction.valueOf("2E2"));
		assertEquals(Fraction.valueOf("0"), Fraction.valueOf("0,0"));
		
		assertEquals(this.f1_2, Fraction.valueOf(0.5));
	}
	
	@Test
	public void testLessOrEqual() {
		assertTrue(Fraction.valueOf(0).lessOrEqual(Fraction.valueOf("0/80")));
		
		assertTrue(this.fn1_2.lessOrEqual(this.f1_2));
		assertFalse(this.f1_2.lessOrEqual(this.fn1_2));
		assertTrue(this.fn1_2.lessOrEqual(this.fn1_2));
		assertTrue(this.f1_2.lessOrEqual(this.f1_2));
		
		assertTrue(this.f1_4.lessOrEqual(this.f1_2));
		assertFalse(this.f1_2.lessOrEqual(this.f1_4));
		
		assertTrue(Fraction.valueOf(0).lessOrEqual(this.f1_2));
		assertTrue(this.fn1_2.lessOrEqual(Fraction.valueOf(0)));
		
	}
}
