package fraction;

public class FractionConstructionException extends RuntimeException {
	public FractionConstructionException(final String string) {
		super(string);
	}
	
	private static final long serialVersionUID = -3123610656916191394L;
}
