package fraction;

import java.math.BigInteger;

public class Fraction implements Comparable<Fraction> {
	private static final String REGEX_SCIENCE_FRACTION = "-?[0-9]+E-?[0-9]+";
	private static final String REGEX_DECIMAL_FRACTION = "-?[0-9]*[.,][0-9]+";
	private static final String REGEX_SINGLENUM_FRACTION = "-?[0-9]+\\.?";
	private static final String REGEX_SLASH_SEPERATED_FRACTION = "-?[0-9]*/-?[0-9]+";
	private static final String REGEX_FRACTION_SEPERATOR = "\\.|/|,|E";
	public static final Fraction ONE = Fraction.valueOf(1);
	public static final Fraction ZERO = Fraction.valueOf(0);
	
	/**
	 * * Creates a new normalized Fraction * * @param enumerator * enumerator of
	 * the Fraction * @param denominator * denominator of the Fraction * @return
	 * normalized Fraction * @throws FractionConstructionException
	 */
	public static Fraction create(final BigInteger enumerator, final BigInteger denominator)
			throws FractionConstructionException {
		if (denominator.equals(BigInteger.ZERO)) {
			throw new FractionConstructionException("0 im Nenner!!!!!");
		}
		return new Fraction(enumerator, denominator).normalize();
	}
	
	/**
	 * * normalizes the provided Fraction * * @return a new normalized Fraction
	 */
	private Fraction normalize() {
		final BigInteger ggt = this.denominator.gcd(this.enumerator);
		if (this.denominator.signum() == -1) {
			return new Fraction(this.enumerator.divide(ggt).multiply(BigInteger.valueOf(-1)),
					this.denominator.divide(ggt).multiply(BigInteger.valueOf(-1)));
		}
		return new Fraction(this.enumerator.divide(ggt), this.denominator.divide(ggt));
	}
	
	@Override
	public int hashCode() {
		return this.normalize().enumerator.hashCode();
	}
	
	/**
	 * * creates a new normalized Fraction represented by the provided String *
	 * * @param fraction * provided String * @return normalized Fraction
	 * * @throws FractionConstructionException
	 */
	public static Fraction valueOf(final String fraction)
			throws FractionConstructionException, IllegalArgumentException {
		final String[] split = fraction.split(REGEX_FRACTION_SEPERATOR);
		String frontPart = null;
		String rearPart = null;
		if (split.length > 1) {
			frontPart = split[0];
			rearPart = split[1];
		}
		switch (getFractionStyle(fraction)) {
		case decimal:
			return Fraction.create(new BigInteger(frontPart + rearPart), BigInteger.TEN.pow(rearPart.length()));
		case scienceStyle:
			if (rearPart.substring(0, 1).equals("-")) {
				return Fraction.create(new BigInteger(frontPart),
						BigInteger.TEN.pow(Integer.valueOf(rearPart.substring(1))));
			} else {
				return Fraction.create(
						new BigInteger(frontPart).multiply(BigInteger.TEN.pow(Integer.valueOf(rearPart))),
						BigInteger.ONE);
			}
		case singleNum:
			return Fraction.create(new BigInteger(fraction), BigInteger.ONE);
		case slashSeperated:
			return Fraction.create(frontPart.length() == 0 ? BigInteger.ONE : new BigInteger(frontPart),
					new BigInteger(rearPart));
		}
		throw new Error();
	}
	
	/**
	 * * Checks the provided String for the different Fraction-Styles that can
	 * be * provided in a String * * @param inputString * the provided String
	 * * @return the correct FractionStyle
	 */
	private static EnumFractionStyle getFractionStyle(final String inputString) throws IllegalArgumentException {
		if (inputString.matches(REGEX_SLASH_SEPERATED_FRACTION)) {
			return EnumFractionStyle.slashSeperated;
		} else if (inputString.matches(REGEX_SINGLENUM_FRACTION)) {
			return EnumFractionStyle.singleNum;
		} else if (inputString.matches(REGEX_DECIMAL_FRACTION)) {
			return EnumFractionStyle.decimal;
		} else if (inputString.matches(REGEX_SCIENCE_FRACTION)) {
			return EnumFractionStyle.scienceStyle;
		} else {
			throw new IllegalArgumentException("Die Eingabe " + inputString + " ist ungültig!");
		}
	}
	
	/**
	 * * <marquee>returns the Fraction represented by the provided *
	 * Number</marquee> * * @param value * provided number * @return normalized
	 * Fraction
	 */
	public static Fraction valueOf(final Number value) {
		try {
			return Fraction.valueOf(value.toString());
		} catch (final FractionConstructionException e) {
			throw new Error();
		}
	}
	
	private enum EnumFractionStyle {
		slashSeperated, decimal, scienceStyle, singleNum;
	}
	
	private final BigInteger enumerator;
	private final BigInteger denominator;
	
	/**
	 * * Constructor of the class * * @param enumerator * enumerator of the
	 * Fraction * @param denominator * denominator of the Fraction
	 */
	private Fraction(final BigInteger enumerator, final BigInteger denominator) {
		this.enumerator = enumerator;
		this.denominator = denominator;
	}
	
	@Override
	public boolean equals(final Object object) {
		if (object instanceof Fraction) {
			final Fraction arg = (Fraction) object;
			return this.enumerator.multiply(arg.denominator).equals(arg.enumerator.multiply(this.denominator));
		} else {
			return false;
		}
	}
	
	/**
	 * * Simple addition on Fraction * * @param summand * summand added to this
	 * * @return this + summand
	 */
	public Fraction add(final Fraction summand) {
		try {
			return Fraction.create(
					this.enumerator.multiply(summand.denominator).add(this.denominator.multiply(summand.enumerator)),
					this.denominator.multiply(summand.denominator));
		} catch (final FractionConstructionException e) {
			throw new Error(e);
		}
	}
	
	/**
	 * * Simple multiplication on Fraction * * @param factor * factor multiplied
	 * with this * @return this * factor
	 */
	public Fraction multiply(final Fraction factor) {
		try {
			return Fraction.create(this.enumerator.multiply(factor.enumerator),
					this.denominator.multiply(factor.denominator));
		} catch (final FractionConstructionException e) {
			throw new Error(e);
		}
	}
	
	/**
	 * * Simple substraction on Fraction * * @param subtrahend * subtrahend to
	 * be substracted from this * @return this - subtrahend
	 */
	public Fraction substract(final Fraction subtrahend) {
		try {
			return Fraction.create(
					this.enumerator.multiply(subtrahend.denominator)
							.subtract(this.denominator.multiply(subtrahend.enumerator)),
					this.denominator.multiply(subtrahend.denominator));
		} catch (final FractionConstructionException e) {
			throw new Error(e);
		}
	}
	
	/**
	 * * Simple division on Fraction * * @param divisor * divisor to be divided
	 * from this * @return this / divisor * @throws
	 * FractionConstructionException
	 */
	public Fraction divide(final Fraction divisor) throws FractionConstructionException {
		return this.multiply(divisor.inversion());
	}
	
	/**
	 * * @param exponent * the exponent to which this Fraction is to be raised
	 * * @return a Fraction whose value is (thise<sup>xponent<sup>) * @throws
	 * FractionConstructionException
	 */
	public Fraction pow(final BigInteger exponent) throws FractionConstructionException {
		if (exponent.equals(BigInteger.ZERO)) {
			return Fraction.valueOf(1);
		} else {
			Fraction value;
			value = exponent.signum() < 0 ? this.inversion() : this;
			final Fraction divideAndConquer = value.pow(exponent.divide(BigInteger.valueOf(2)));
			final Fraction preResult = divideAndConquer.multiply(divideAndConquer);
			return exponent.mod(BigInteger.valueOf(2)).equals(BigInteger.ZERO) ? preResult : preResult.multiply(this);
		}
	}
	
	public static BigInteger harmony(final BigInteger m) throws FractionConstructionException {
		Fraction value = Fraction.ZERO;
		BigInteger counter = BigInteger.ZERO;
		while (value.compareTo(Fraction.valueOf(m)) < 0) {
			counter = counter.add(BigInteger.ONE);
			value = value.add(Fraction.create(BigInteger.ONE, counter));
		}
		return counter;
	}
	
	/**
	 * * @return Kehrwert des Bruches * @throws FractionConstructionException
	 */
	public Fraction inversion() throws FractionConstructionException {
		return Fraction.create(this.denominator, this.enumerator);
	}
	
	@Override
	public String toString() {
		return this.enumerator + "/" + this.denominator;
	}
	
	public boolean lessOrEqual(final Fraction arg) {
		return this.compareTo(arg) <= 0;
	}
	
	@Override
	public int compareTo(final Fraction arg) {
		return this.enumerator.multiply(arg.denominator).compareTo(arg.enumerator.multiply(this.denominator));
	}
}
