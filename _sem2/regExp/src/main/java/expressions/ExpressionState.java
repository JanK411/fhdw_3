package expressions;

import automaton.Automaton;

public interface ExpressionState {

	void setIterated(RegularExpression regularExpression);

	void setOptional(RegularExpression regularExpression);
	
	void doThings(Automaton aut);
	
}
