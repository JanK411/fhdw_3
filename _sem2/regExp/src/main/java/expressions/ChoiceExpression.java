package expressions;

import java.util.LinkedList;
import java.util.List;

import automaton.Automaton;

public class ChoiceExpression extends ListExpression {
	
	private ChoiceExpression(final List<RegularExpression> content) {
		super(content);
	}
	
	public static ChoiceExpression create() {
		return new ChoiceExpression(new LinkedList<>());
	}
	
	@Override
	protected Automaton toStandardAutomaton() {
		return Automaton.createChoiceAutomaton(this.getAutomatons());
	}
	
}
