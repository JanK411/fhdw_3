package expressions;

import java.util.LinkedList;
import java.util.List;

import automaton.Automaton;

public class SequenceExpression extends ListExpression {
	
	private SequenceExpression(final List<RegularExpression> content) {
		super(content);
	}
	
	public static SequenceExpression create() {
		return new SequenceExpression(new LinkedList<>());
	}
	
	
	@Override
	protected Automaton toStandardAutomaton() {
		return Automaton.createSequenceAutomaton(this.getAutomatons());
	}
	
}
