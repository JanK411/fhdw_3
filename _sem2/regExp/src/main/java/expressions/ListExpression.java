package expressions;

import java.util.LinkedList;
import java.util.List;

import automaton.Automaton;

public abstract class ListExpression extends RegularExpression {
	private final List<RegularExpression> content;
	
	protected ListExpression(final List<RegularExpression> content) {
		super();
		this.content = content;
	}
	
	public void add(final RegularExpression expression) {
		
		this.getContent().add(expression);
	}
	
	public List<RegularExpression> getContent() {
		return this.content;
	}
	
	protected List<Automaton> getAutomatons() {
		final List<Automaton> ret = new LinkedList<>();
		for (final RegularExpression current : this.content) {
			ret.add(current.toStandardAutomaton());
		}
		return ret;
	}
}
