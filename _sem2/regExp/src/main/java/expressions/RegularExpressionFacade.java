package expressions;

public class RegularExpressionFacade {
	
	public static RegularExpressionFacade create() {
		return new RegularExpressionFacade();
	}
	
	/**
	 * @return RegularBaseExpression, the language of which is {c}
	 */
	public BaseExpression createBaseExpression(final char c) {
		System.out.println("Base:" + c);
		return BaseExpression.create(c);
	}
	
	/**
	 * @return RegularExpression, the language of which is the empty set
	 */
	public ChoiceExpression createChoiceExpression() {
		System.out.println("ChoiceExpression");
		return ChoiceExpression.create();
	}
	
	/**
	 * @return RegularExpression, the language of which is {""}
	 */
	public SequenceExpression createSequenceExpression() {
		System.out.println("SequenceExpression");
		return SequenceExpression.create();
	}
	
	/**
	 * Adds the containee as a direct subexpression to the container. Order of
	 * addition is significant!
	 * 
	 * @param container
	 *            The whole
	 * @param containee
	 *            The part
	 */
	public void add(final ListExpression container, final RegularExpression containee) {
		container.add(containee);
		System.out.println("add");
	}
	
	public void setIterated(final RegularExpression expression) {
		expression.setIterated();
		System.out.println("iterated");
	}
	
	public void setOptional(final RegularExpression result) {
		result.setOptional();
		System.out.println("optional");
	}
	
}
