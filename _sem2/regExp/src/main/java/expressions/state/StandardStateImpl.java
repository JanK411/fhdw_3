package expressions.state;

import automaton.Automaton;
import expressions.RegularExpression;

public class StandardStateImpl implements StandardState {
	
	protected StandardStateImpl() {
	}
	
	@Override
	public void setIterated(final RegularExpression regularExpression) {
		regularExpression.setState(new IteratedStateImpl());
	}
	
	@Override
	public void setOptional(final RegularExpression regularExpression) {
		regularExpression.setState(new OptionalStateImpl());
	}

	@Override
	public void doThings(final Automaton aut) {
		//Done.
	}
	
}
