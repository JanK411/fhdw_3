package expressions.state;

import expressions.ExpressionState;

public interface StandardState extends ExpressionState {
	
	static StandardState instance = new StandardStateImpl();
	
	static StandardState getInstance() {
		return instance;
	}
	
}
