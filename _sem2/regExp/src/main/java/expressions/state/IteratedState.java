package expressions.state;

import automaton.Automaton;
import expressions.ExpressionState;

public interface IteratedState extends ExpressionState {
	public void doIteratedThings(final Automaton aut);
}
