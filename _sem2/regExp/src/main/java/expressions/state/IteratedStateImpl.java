package expressions.state;

import automaton.Automaton;
import expressions.RegularExpression;

public class IteratedStateImpl implements IteratedState {
	
	@Override
	public void doIteratedThings(final Automaton aut) {
		aut.setIterated();
	}
	
	@Override
	public void setIterated(final RegularExpression regularExpression) {
		// Done.
		
	}

	@Override
	public void setOptional(final RegularExpression regularExpression) {
		regularExpression.setState(new BothStateImpl());
	}

	@Override
	public void doThings(final Automaton aut) {
		this.doIteratedThings(aut);
	}
	
}
