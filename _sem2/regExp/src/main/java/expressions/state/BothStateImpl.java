package expressions.state;

import automaton.Automaton;
import expressions.RegularExpression;

public class BothStateImpl implements BothState {
	
	private final IteratedState iteratedState = new IteratedStateImpl();
	private final OptionalState optionalState = new OptionalStateImpl();
	
	@Override
	public void doIteratedThings(final Automaton aut) {
		this.iteratedState.doIteratedThings(aut);
	}
	
	@Override
	public void doOptionalThings(final Automaton aut) {
		this.optionalState.doOptionalThings(aut);
	}

	@Override
	public void setIterated(final RegularExpression regularExpression) {
		// Done.
	}

	@Override
	public void setOptional(final RegularExpression regularExpression) {
		// Done as well.
	}

	@Override
	public void doThings(final Automaton aut) {
		this.doIteratedThings(aut);
		this.doOptionalThings(aut);
	}
	
}
