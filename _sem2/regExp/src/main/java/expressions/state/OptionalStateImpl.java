package expressions.state;

import automaton.Automaton;
import expressions.RegularExpression;

public class OptionalStateImpl implements OptionalState {
	
	
	@Override
	public void setIterated(final RegularExpression regularExpression) {
		regularExpression.setState(new BothStateImpl());
	}
	
	@Override
	public void setOptional(final RegularExpression regularExpression) {
		// Done.
	}

	@Override
	public void doThings(final Automaton aut) {
		this.doOptionalThings(aut);
	}

	@Override
	public void doOptionalThings(final Automaton aut) {
		aut.setOptional();
	}
	
}
