package expressions.state;

import automaton.Automaton;
import expressions.ExpressionState;

public interface OptionalState extends ExpressionState {
	public void doOptionalThings(final Automaton aut);
}
