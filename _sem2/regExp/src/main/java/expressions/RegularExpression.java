package expressions;

import automaton.Automaton;
import expressions.state.StandardState;

public abstract class RegularExpression {
	
	ExpressionState state;
	
	protected RegularExpression() {
		this.state = StandardState.getInstance();
	}
	
	/**
	 * @param text
	 * @return true if and only if the text belongs to the language of the
	 *         receiver.
	 */
	public boolean check(final String text) {
		final Automaton aut = this.toAutomaton();
		return aut.recognizes(text);
	}
	
	private Automaton toAutomaton() {
		final Automaton aut = this.toStandardAutomaton();
		this.state.doThings(aut);
		return aut;
	}
	
	protected abstract Automaton toStandardAutomaton();
	
	public void setIterated() {
		this.state.setIterated(this);
	}
	
	public void setOptional() {
		this.state.setOptional(this);
	}
	
	public void setState(final ExpressionState state) {
		this.state = state;
	}
	
}
