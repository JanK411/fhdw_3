package expressions;

import automaton.Automaton;

public class BaseExpression extends RegularExpression {
	private final char c;
	
	private BaseExpression(final char c) {
		this.c = c;
	}
	
	public static BaseExpression create(final char c) {
		return new BaseExpression(c);
	}
	
	@Override
	protected Automaton toStandardAutomaton() {
		final Automaton aut = Automaton.createAutomatonRecognizing(this.c);
		return aut;
	}
}
