package regExp;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import automaton.State;
import expressions.RegularExpression;
import parser.RegularExpressionParser;
import parser.RegularExpressionParserException;

public class ReguarExpressionStarterTest {
	
	@Test
	public void testSeq() throws RegularExpressionParserException {
		final RegularExpression abcExp = RegularExpressionParser.create("(abc)").parse();
		assertTrue(abcExp.check("abc"));
		assertFalse(abcExp.check("abcabc"));
		assertFalse(abcExp.check(""));
		
		final RegularExpression abcPExp = RegularExpressionParser.create("(abc)+").parse();
		assertTrue(abcPExp.check("abc"));
		assertTrue(abcPExp.check("abcabc"));
		assertFalse(abcPExp.check(""));
		
		final RegularExpression abcSExp = RegularExpressionParser.create("(abc)*").parse();
		assertTrue(abcSExp.check("abc"));
		assertTrue(abcSExp.check("abcabc"));
		assertTrue(abcSExp.check(""));
		
		final RegularExpression abcOExp = RegularExpressionParser.create("(abc)?").parse();
		assertTrue(abcOExp.check("abc"));
		assertFalse(abcOExp.check("abcabc"));
		assertTrue(abcOExp.check(""));
		
	}
	
	@Test
	public void testCho() throws RegularExpressionParserException {
		final RegularExpression abcExp = RegularExpressionParser.create("[abc]").parse();
		assertTrue(abcExp.check("a"));
		assertTrue(abcExp.check("b"));
		assertTrue(abcExp.check("c"));
		assertFalse(abcExp.check("f"));
		assertFalse(abcExp.check("abc"));
		assertFalse(abcExp.check("abcabc"));
		assertFalse(abcExp.check("abcabcababcabcabcabcbacbacbbcababcbacbabc"));
		assertFalse(abcExp.check(""));
		
		final RegularExpression abcPExp = RegularExpressionParser.create("[abc]+").parse();
		assertTrue(abcPExp.check("a"));
		assertTrue(abcPExp.check("b"));
		assertTrue(abcPExp.check("c"));
		assertFalse(abcPExp.check("f"));
		assertTrue(abcPExp.check("abc"));
		assertTrue(abcPExp.check("abcabc"));
		assertTrue(abcPExp.check("abcabbbbbbbcababcabcabcabcbacbacbbcababcbacbabc"));
		assertTrue(abcPExp.check("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"));
		assertFalse(abcPExp.check(""));
		
		final RegularExpression abcSExp = RegularExpressionParser.create("[abc]*").parse();
		assertTrue(abcSExp.check("a"));
		assertTrue(abcSExp.check("b"));
		assertTrue(abcSExp.check("c"));
		assertFalse(abcSExp.check("f"));
		assertTrue(abcSExp.check("abc"));
		assertTrue(abcSExp.check("abcabc"));
		assertTrue(abcSExp.check("abcabbbbbbbcababcabcabcabcbacbacbbcababcbacbabc"));
		assertTrue(abcSExp.check("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"));
		assertTrue(abcSExp.check(""));
		
		final RegularExpression abcOExp = RegularExpressionParser.create("[abc]?").parse();
		assertTrue(abcOExp.check("a"));
		assertTrue(abcOExp.check("b"));
		assertTrue(abcOExp.check("c"));
		assertFalse(abcOExp.check("f"));
		assertFalse(abcOExp.check("abc"));
		assertFalse(abcOExp.check("bc"));
		assertFalse(abcOExp.check("abcabc"));
		assertTrue(abcOExp.check(""));
		
	}
	
	@Test
	public void testMulti() throws RegularExpressionParserException {
		State.i = 0;
		final RegularExpression multiExp = RegularExpressionParser.create("((abc)[de]+[abc]*(nk)+)").parse();
		System.out.println(multiExp);
		assertTrue(multiExp.check("abcdededdddddddeeeeeeeeeednknk"));
		assertTrue(multiExp.check("abcdededdddddddeeeeeeeeeedccccccccccccnknk"));
		assertTrue(multiExp.check("abcdededdddddddeeeeeeeeeedcccccccbbbbbbacccccnknk"));
		
	}
	
}
